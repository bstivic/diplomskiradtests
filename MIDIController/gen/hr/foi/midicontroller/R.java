/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package hr.foi.midicontroller;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int background=0x7f050001;
        public static final int dark_orange=0x7f050000;
        public static final int white=0x7f050002;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Example customization of dimensions originally defined in res/values/dimens.xml
         (such as screen margins) for screens with more than 820dp of available width. This
         would include 7" and 10" devices in landscape (~960dp and ~1280dp respectively).
    
         */
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
    }
    public static final class drawable {
        public static final int background=0x7f020000;
        public static final int button_normal_off=0x7f020001;
        public static final int button_normal_on=0x7f020002;
        public static final int button_pressed=0x7f020003;
        public static final int flat_key_normal=0x7f020004;
        public static final int flat_key_pressed=0x7f020005;
        public static final int ic_launcher=0x7f020006;
        public static final int keys_containter=0x7f020007;
        public static final int sharp_key_normal=0x7f020008;
        public static final int sharp_key_pressedl=0x7f020009;
    }
    public static final class id {
        public static final int TlblPitcThresh=0x7f0a0026;
        public static final int action_settings=0x7f0a002f;
        public static final int btnA=0x7f0a001a;
        public static final int btnASharp=0x7f0a001b;
        public static final int btnBack=0x7f0a0009;
        public static final int btnC=0x7f0a0011;
        public static final int btnCSharp=0x7f0a0012;
        public static final int btnConnected=0x7f0a000a;
        public static final int btnD=0x7f0a0013;
        public static final int btnDSharp=0x7f0a0014;
        public static final int btnE=0x7f0a0015;
        public static final int btnExit=0x7f0a002e;
        public static final int btnF=0x7f0a0016;
        public static final int btnFSharp=0x7f0a0017;
        public static final int btnG=0x7f0a0018;
        public static final int btnGSharp=0x7f0a0019;
        public static final int btnH=0x7f0a001c;
        public static final int btnKeyboard=0x7f0a002c;
        public static final int btnMode=0x7f0a0002;
        public static final int btnModulation=0x7f0a001e;
        public static final int btnNext=0x7f0a000e;
        public static final int btnOctave1=0x7f0a0006;
        public static final int btnOctave2=0x7f0a0005;
        public static final int btnOctave3=0x7f0a0004;
        public static final int btnOctave4=0x7f0a0007;
        public static final int btnOctave5=0x7f0a0008;
        public static final int btnPause=0x7f0a000d;
        public static final int btnPitch=0x7f0a0003;
        public static final int btnPlay=0x7f0a000b;
        public static final int btnPrev=0x7f0a0010;
        public static final int btnRecord=0x7f0a000f;
        public static final int btnReset=0x7f0a001d;
        public static final int btnSettings=0x7f0a002d;
        public static final int btnStop=0x7f0a000c;
        public static final int fragment_container=0x7f0a0000;
        public static final int imageView1=0x7f0a0020;
        public static final int keys_fragment=0x7f0a0001;
        public static final int lblChannel=0x7f0a0022;
        public static final int lblModSens=0x7f0a002a;
        public static final int lblModThresh=0x7f0a0021;
        public static final int lblPitchSens=0x7f0a0028;
        public static final int seedPitchThre=0x7f0a0024;
        public static final int seekMThresh=0x7f0a0027;
        public static final int seekModSens=0x7f0a0025;
        public static final int seekPitchSens=0x7f0a0029;
        public static final int settings_fragment=0x7f0a001f;
        public static final int spinnerChannel=0x7f0a0023;
        public static final int splash_fragment=0x7f0a002b;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int keys_fragment=0x7f030001;
        public static final int settings_fragment=0x7f030002;
        public static final int splash_fragment=0x7f030003;
    }
    public static final class menu {
        public static final int main=0x7f090000;
    }
    public static final class string {
        public static final int action_settings=0x7f070002;
        public static final int app_name=0x7f070000;
        public static final int hello_world=0x7f070001;
        public static final int keyboard_back=0x7f070013;
        public static final int keyboard_connected=0x7f070014;
        public static final int keyboard_mode=0x7f07001b;
        public static final int keyboard_modulation=0x7f070012;
        public static final int keyboard_octave_1=0x7f070015;
        public static final int keyboard_octave_2=0x7f070016;
        public static final int keyboard_octave_3=0x7f070017;
        public static final int keyboard_octave_4=0x7f070018;
        public static final int keyboard_octave_5=0x7f070019;
        /**  FRAGMENT KEYBOARD 
         */
        public static final int keyboard_pitch=0x7f070011;
        public static final int keyboard_poly=0x7f07001a;
        public static final int keyboard_reset=0x7f07001c;
        /**  MAIN ACTIVITY 
         */
        public static final int main_activity=0x7f070003;
        /**  FRAGMENT SETTINGS 
         */
        public static final int settings_back=0x7f07000a;
        public static final int settings_chanel=0x7f07000f;
        public static final int settings_mod_sensitivity=0x7f07000c;
        public static final int settings_mod_treshold=0x7f07000b;
        public static final int settings_pitch_sensitivity=0x7f07000e;
        public static final int settings_pitch_threshold=0x7f07000d;
        public static final int settings_title=0x7f070010;
        public static final int splash_button_description=0x7f070005;
        public static final int splash_controller=0x7f070007;
        public static final int splash_exit=0x7f070009;
        /**  FRAGMENT SPLASH 
         */
        public static final int splash_image_description=0x7f070004;
        public static final int splash_keyboard=0x7f070006;
        public static final int splash_settings=0x7f070008;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f080000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f080001;
        public static final int mySpinnerItemStyle=0x7f080002;
    }
    public static final class xml {
        public static final int accessory_filter=0x7f040000;
    }
}
