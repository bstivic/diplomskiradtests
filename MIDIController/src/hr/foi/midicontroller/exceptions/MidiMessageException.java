package hr.foi.midicontroller.exceptions;

public class MidiMessageException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public MidiMessageException(String message) {
		super(message);
	}
	
	public MidiMessageException() {
		
	}
}
