package hr.foi.midicontroller.ui;

import hr.foi.midicontroller.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;

import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ToggleButton;


@SuppressLint("ClickableViewAccessibility")
public class KeysFragment extends Fragment {
	private MainActivity activity;
	public static final String TAG = "KeysFragment";
	OnButtonPressedListener mCallback;
	private Rect rect;
	//Buttons
	Button btnBack, btnI, btnII, btnIII, btnIV, btnV, btnReset;
	ToggleButton btnConnected;
	//keys
	private Button 	btnC, btnCS, btnD, btnDS, btnE, btnF, btnFS, 
					btnG, btnGS, btnA, btnAS, btnH;
	
	//MMC controls
	private Button 	btnPlay, btnPause, btnStop, btnRecord,
					btnPrevious, btnNext;
	
	//Modulation and pitch
	private Button btnPitch;
	private Button btnModulation;
	
	//================================================================================
    // Enumerations
    //================================================================================
	public enum Note {
		C(60), 		
		CSharp (61),		
		D(62),			
		DSharp(63),	
		E(64),	
		F(65),	
		FSharp(66),	
		G(67),			
		GSharp(68),		
		A(69),
		ASharp(70),
		H(71),
		Pitch(220),
		Modulation(230);
		private int value;
        private Note(int value){
        	this.value = value;
        }        
        public int getValue(){
        	return this.value;
        }
	};   
	
	
	//================================================================================
    // Interface
    //================================================================================
	public interface OnButtonPressedListener {
		public void onBackPressed();
		public void onConnectedPressed();
		public void onOctaveIPressed();
		public void onOctaveIIPressed();
		public void onOctaveIIIPressed();
		public void onOctaveIVPressed();
		public void onOctaveVPressed();
		
		public void onNotePressed(int note, boolean down);
		public void onPlayPressed();
		public void onPausePressed();
		public void onRecordPressed();
		public void onStopPressed();
		public void onNextPressed();
		public void onPreviousPressed();
		public void onPitchBendReset();
		public void onPitchBend(float bend);
		public void onResetPressed();
		public void onModulationChange(int change);
		public void onModulationReset();
		
	}
	
	
	//================================================================================
    // Life-cycle
    //================================================================================
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View inflaterView  = inflater.inflate(R.layout.keys_fragment, container, false);		
		
		btnBack = (Button) inflaterView.findViewById(R.id.btnBack);		
		btnI = (Button) inflaterView.findViewById(R.id.btnOctave1);		
		btnII = (Button) inflaterView.findViewById(R.id.btnOctave2);		
		btnIII = (Button) inflaterView.findViewById(R.id.btnOctave3);		
		btnIV = (Button) inflaterView.findViewById(R.id.btnOctave4);		
		btnV = (Button) inflaterView.findViewById(R.id.btnOctave5);	
		
		btnC = (Button) inflaterView.findViewById(R.id.btnC);		
		btnCS = (Button) inflaterView.findViewById(R.id.btnCSharp);		
		btnD = (Button) inflaterView.findViewById(R.id.btnD);		
		btnDS = (Button) inflaterView.findViewById(R.id.btnDSharp);		
		btnE= (Button) inflaterView.findViewById(R.id.btnE);		
		btnF = (Button) inflaterView.findViewById(R.id.btnF);		
		btnFS = (Button) inflaterView.findViewById(R.id.btnFSharp);	
		btnG = (Button) inflaterView.findViewById(R.id.btnG);		
		btnGS = (Button) inflaterView.findViewById(R.id.btnGSharp);		
		btnA = (Button) inflaterView.findViewById(R.id.btnA);	
		btnAS = (Button) inflaterView.findViewById(R.id.btnASharp);		
		btnH = (Button) inflaterView.findViewById(R.id.btnH);		
		
		btnC.setId(Note.C.value);
		btnCS.setId(Note.CSharp.value);		
		btnD.setId(Note.D.value);	
		btnDS.setId(Note.DSharp.value);		
		btnE.setId(Note.E.value);
		btnF.setId(Note.F.value);
		btnFS.setId(Note.FSharp.value);
		btnG.setId(Note.G.value);		
		btnGS.setId(Note.GSharp.value);
		btnA.setId(Note.A.value);
		btnAS.setId(Note.ASharp.value);
		btnH.setId(Note.H.value);
		
		NoteTouchListener noteTouchListener = new NoteTouchListener();
		
		btnC.setOnTouchListener(noteTouchListener);
		btnCS.setOnTouchListener(noteTouchListener);
		btnD.setOnTouchListener(noteTouchListener);
		btnDS.setOnTouchListener(noteTouchListener);
		btnE.setOnTouchListener(noteTouchListener);
		btnF.setOnTouchListener(noteTouchListener);
		btnFS.setOnTouchListener(noteTouchListener);
		btnG.setOnTouchListener(noteTouchListener);	
		btnGS.setOnTouchListener(noteTouchListener);
		btnA.setOnTouchListener(noteTouchListener);
		btnAS.setOnTouchListener(noteTouchListener);
		btnH.setOnTouchListener(noteTouchListener);
		
		
		btnPlay = (Button) inflaterView.findViewById(R.id.btnPlay);		
		btnPause = (Button) inflaterView.findViewById(R.id.btnPause);		
		btnStop = (Button) inflaterView.findViewById(R.id.btnStop);		
		btnRecord = (Button) inflaterView.findViewById(R.id.btnRecord);		
		btnNext = (Button) inflaterView.findViewById(R.id.btnNext);		
		btnPrevious = (Button) inflaterView.findViewById(R.id.btnPrev);		
		btnReset = (Button)inflaterView.findViewById(R.id.btnReset);	
		
		btnConnected = (ToggleButton) inflaterView.findViewById(R.id.btnConnected);
		
		//status
		btnBack.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {backToSplash();}});
		btnConnected.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {changeConnectStatus();}});
		btnReset.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {mCallback.onResetPressed();}});
		
		//octave
		btnI.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {onOctaveIPressed();}});
		btnII.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {onOctaveIIPressed();}});
		btnIII.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {onOctaveIIIPressed();}});
		btnIV.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {onOctaveIVPressed();}});
		btnV.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {onOctaveVPressed();}});
		
		
		btnC.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnC.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnCS.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnD.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnDS.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnE.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnF.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnFS.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnG.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnGS.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnA.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnAS.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		btnH.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {}});
		
		btnPlay.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {mCallback.onPlayPressed();}});	
		btnPause.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {mCallback.onPausePressed();}});		
		btnStop.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {mCallback.onStopPressed();}});		
		btnRecord.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {mCallback.onRecordPressed();}});		
		btnNext.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {mCallback.onNextPressed();}});		
		btnPrevious.setOnClickListener(new OnClickListener() {@Override public void onClick(View v) {mCallback.onPreviousPressed();}});		
		
	
		//mode
		PitchTouchListener pitchTouch = new PitchTouchListener();
		btnPitch = (Button) inflaterView.findViewById(R.id.btnPitch);		
		btnPitch.setOnTouchListener(pitchTouch);
		btnPitch.setId(Note.Pitch.value);
		
		ModulationTouchListener modulationTouch = new ModulationTouchListener();
		btnModulation = (Button) inflaterView.findViewById(R.id.btnModulation);		
		btnModulation.setOnTouchListener(modulationTouch);
		btnModulation.setId(Note.Modulation.value);
				
		return inflaterView;
	}
	

	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity) activity;
        try {
            mCallback = (OnButtonPressedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnButtonPressedListener");
        }
    }
	
	//================================================================================
    // Properties
    //================================================================================
	public boolean getbtnConnectedState(){
		return btnConnected.isChecked();
	}
	public void setbtnConnectedState(boolean state){
		btnConnected.setChecked(state);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		activity.setTextColorBtnOctave(activity.app.getOctave(), Color.WHITE);		
	};
	
	
	@Override
	public void onResume() {		
		super.onResume();
		btnConnected.setChecked(activity.app.isConnected());
	}
	//**************************************************************************************************
	//PRIVATE METHODS
	//**************************************************************************************************
	private void backToSplash() {
		mCallback.onBackPressed();
	};
	
	public void changeConnectStatus(){
		mCallback.onConnectedPressed();
	}
	public void onOctaveIPressed(){
		mCallback.onOctaveIPressed();
	}
	public void onOctaveIIPressed(){
		mCallback.onOctaveIIPressed();
	}
	public void onOctaveIIIPressed(){
		mCallback.onOctaveIIIPressed();
	}
	public void onOctaveIVPressed(){
		mCallback.onOctaveIVPressed();
	}
	public void onOctaveVPressed(){
		mCallback.onOctaveVPressed();
	}

	//**************************************************************************************************
	//CLASS TOUCH LISTENERS
	//**************************************************************************************************
	public class NoteTouchListener implements OnTouchListener{
	
		@Override
		public boolean onTouch(View arg0, MotionEvent arg1) {
			int action = MotionEventCompat.getActionMasked(arg1);
			if(arg0.getId() >= Note.C.value && arg0.getId() <= Note.H.value ){
				switch (action) {
					case (MotionEvent.ACTION_DOWN):
						mCallback.onNotePressed(arg0.getId(), true);
					 	rect = new Rect(arg0.getLeft(),arg0.getTop(), arg0.getRight(), arg0.getBottom());
						break;
					case (MotionEvent.ACTION_UP):
						mCallback.onNotePressed(arg0.getId(), false);
						arg0.performClick();
						break;	
					case (MotionEvent.ACTION_MOVE):
						if(!rect.contains(arg0.getLeft() + (int) arg1.getX(), arg0.getTop() + (int) arg1.getY())){
							mCallback.onNotePressed(arg0.getId(), false);
							arg0.performClick();
				        }
						
						break;	

				}
			}
			return false;
		}
	}
	
	public class PitchTouchListener implements OnTouchListener{
		private static final int INVALID_POINTER_ID = -1;
		private int mActivePointerId = INVALID_POINTER_ID;
		
		private float mLastTouchY;
		
		private float firstTouchPointY;
	
		
		
		@Override
		public boolean onTouch(View arg0, MotionEvent arg1) {
			int action = MotionEventCompat.getActionMasked(arg1);
			if(arg0.getId() == Note.Pitch.value){
				switch (action) {
					case (MotionEvent.ACTION_DOWN):{
						beginPitch(arg1);
						break;
					}
					case(MotionEvent.ACTION_MOVE):{
						//get coordinates of pointer
						final int pointerIndex = arg1.findPointerIndex(mActivePointerId);						
						final float y = arg1.getY(pointerIndex);
						
						calculatePitch(y);
				
						break;
					}
					case (MotionEvent.ACTION_UP):{
						finishPitch();
						break;	
					}
					case (MotionEvent.ACTION_CANCEL):{
						finishPitch();
						break;	
					}
					
			}
			}
			return true;
		}



		/**
		 * Method will reset state of pitch wheel
		 */
		private void finishPitch() {
			mActivePointerId = INVALID_POINTER_ID;
			Log.d("Touch", "UP!");
			mCallback.onPitchBendReset();
		}



		/***
		 * Method will start to recording moving pointer when user clicks on pitch button
		 * @param arg1
		 */
		private void beginPitch(MotionEvent arg1) {
			//remember coordinates of first touch
			firstTouchPointY = arg1.getY();
			
			mLastTouchY = firstTouchPointY;
			
			//save id of the pointer
			mActivePointerId = arg1.getPointerId(0);
			
			Log.d("Touch", "Touch down - firstTouchPointY: "+firstTouchPointY);
		}



		/**
		 * Method will calculate pitch bend factor -1f <- 0 -> 1f based on user interaction
		 * @param y
		 */
		private void calculatePitch(final float y) {
			if(Math.abs(mLastTouchY-y) > activity.app.getSensitivityTreshold()){
				final float dy = firstTouchPointY - y;
				final float sensitivityFactor;
				
				if(Math.abs(dy) >=  activity.app.getSensitivityFactor()) {
					
					if(dy<0){
						sensitivityFactor =  -1*activity.app.getSensitivityFactor();
					}else{
						sensitivityFactor =  activity.app.getSensitivityFactor();
					}											
				}else{
					sensitivityFactor = dy;
				}
				
				float bend = sensitivityFactor / activity.app.getSensitivityFactor();
				mLastTouchY = y;
				
				Log.d("Touch", "Move - dy: "+dy+" Bend: "+bend);
				
				mCallback.onPitchBend(bend);
			}
		}
	}
	
	
	public class ModulationTouchListener implements OnTouchListener{
		private static final int INVALID_POINTER_ID = -1;
		private int mActivePointerId = INVALID_POINTER_ID;
		
		private float mLastTouchY;
		
		private float firstTouchPointY;
	
		
		
		@Override
		public boolean onTouch(View arg0, MotionEvent arg1) {
			int action = MotionEventCompat.getActionMasked(arg1);
			if(arg0.getId() == Note.Modulation.value){
				switch (action) {
					case (MotionEvent.ACTION_DOWN):{
						startModulate(arg1);
						break;
					}
					case(MotionEvent.ACTION_MOVE):{
						calculateModulation(arg1);
				
						break;
					}
					case (MotionEvent.ACTION_UP):{
						stopModulation();
						break;	
					}
					case (MotionEvent.ACTION_CANCEL):{
						stopModulation();
						break;	
					}
					
			}
			}
			return true;
		}



		/**
		 * Method will reset modulation wheel
		 */
		private void stopModulation() {
			mActivePointerId = INVALID_POINTER_ID;
			Log.d("Touch", "UP!");
			mCallback.onModulationReset();
		}



		/**
		 * Calculates modulation step 0-127 based on user interaction
		 * @param arg1
		 */
		private void calculateModulation(MotionEvent arg1) {
			//get coordinates of pointer
			final int pointerIndex = arg1.findPointerIndex(mActivePointerId);						
			final float y = arg1.getY(pointerIndex);
			
			if(Math.abs(mLastTouchY-y) > activity.app.getSensitivityModulationTreshold()){
				final float dy = firstTouchPointY - y;
				final float sensitivityFactor;
				if(dy>0){
					if(Math.abs(dy) >=  activity.app.getSensitivityModulationFactor()) {
						
							
							sensitivityFactor =  activity.app.getSensitivityModulationFactor();
							
					}else{
						sensitivityFactor = dy;
					}
					
						float bend = sensitivityFactor / activity.app.getSensitivityModulationFactor();
						mLastTouchY = y;
						Log.d("Touch", "Move - dy: "+dy+" Bend: "+bend);
						int modulate = (int) (bend * 127);
						mCallback.onModulationChange(modulate);
					}
			}
		}



		/**
		 * Starts recording when user push button
		 * @param arg1
		 */
		private void startModulate(MotionEvent arg1) {
			//remember coordinates of first touch
			firstTouchPointY = arg1.getY();
			
			mLastTouchY = firstTouchPointY;
			
			//save id of the pointer
			mActivePointerId = arg1.getPointerId(0);
			
			Log.d("Touch", "Touch down - firstTouchPointY: "+firstTouchPointY);
		}
	}
	
}