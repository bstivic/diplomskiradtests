package hr.foi.midicontroller.ui;


import hr.foi.midicontroller.R;
import hr.foi.midicontroller.bridge.IMidiBridge;
import hr.foi.midicontroller.bridge.IoioBridge;
import hr.foi.midicontroller.core.AppData;
import hr.foi.midicontroller.core.MidiHelper;
import hr.foi.midicontroller.core.MidiMessage;
import hr.foi.midicontroller.core.MidiOut;
import hr.foi.midicontroller.core.MidiMessage.MidiType;
import hr.foi.midicontroller.exceptions.MidiMessageException;
import hr.foi.midicontroller.service.IService;
import hr.foi.midicontroller.service.IoioOutService;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ToggleButton;
 
public class MainActivity extends Activity
implements SplashFragment.OnButtonPressedListener,
		KeysFragment.OnButtonPressedListener,
		SettingsFragment.OnButtonPressedListener{
	AppData app;
	//fragments
	private SplashFragment splashFragment;
	private KeysFragment keysFragment;
	private SettingsFragment settingsFragment;
	//broadcasts
	private BroadcastReceiver mReceiverConnected;
	private BroadcastReceiver mReceiverDisconnected;
	private IntentFilter mIntentFilterConnected;
	private IntentFilter mIntentFilterDisconnected;
	
	MidiHelper helper;
	
	//================================================================================
    // Life-Cycle
    //================================================================================
  
	@Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	 //REMOVE TITLE BAR
         this.requestWindowFeature(Window.FEATURE_NO_TITLE);

         //REMOVE NOTIFICATION BAR
         this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
         
         //FORCE LANDSCAPE ORIENTATION
         this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
         setContentView(R.layout.activity_main);  	         
         
         IMidiBridge bridge = new IoioBridge(100);
         IService service = new IoioOutService();
         MidiOut out =  new MidiOut(bridge);
         
         //Storing controllers to appData
         this.app = new AppData(bridge, service, out);
         this.app.setOctave(3);
         //TODO namjesti channel
         this.app.setChannel(1);
         
         helper = new MidiHelper();
         
         initializeSplashFragmetn(savedInstanceState);
         keysFragment = new KeysFragment();		
     	//BROADCAST RECIEVERS
     	mReceiverConnected = new BroadcastReceiver() {
     		@Override 
			public void onReceive(Context context, Intent intent) {
				 onConnect();    		     
    		     }
     	}; 
     	mReceiverDisconnected = new BroadcastReceiver() {
     		@Override
       		 public void onReceive(Context context, Intent intent) {
       		     onDisconnect();  		  
     		}
        };	
    	mIntentFilterConnected=new IntentFilter("midi.board.CONNECTED");
    	mIntentFilterDisconnected=new IntentFilter("midi.board.DISCONNECTED");
	}
	
	@Override
	protected void onResume() {
		super.onResume(); 
		registerReceiver(mReceiverConnected, mIntentFilterConnected);
		registerReceiver(mReceiverDisconnected, mIntentFilterDisconnected);
	}
	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(mReceiverConnected);
		unregisterReceiver(mReceiverDisconnected);
	}
    @Override
    protected void onStart() {
    	Context c =  (Context) this;
    	app.getBridge().connect(c,app.getService());    
    	
    	super.onStart();
    }
    
    @Override
    protected void onStop() {
    	app.getBridge().disconnect();

    	super.onStop();
    }
	
	//================================================================================
    // Private methods
    //================================================================================

	private void initializeSplashFragmetn(Bundle savedInstanceState) {
        if (findViewById(R.id.fragment_container) != null) {
	        if (savedInstanceState != null) {
	            return;
	        }	
		   splashFragment = new SplashFragment();
		   
		   FragmentManager fragmentManager = getFragmentManager();
	       FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	       fragmentTransaction.add(R.id.fragment_container, splashFragment, SplashFragment.TAG);
	       fragmentTransaction.commit();		
        }	 
        
	}
	
	public void setTextColorBtnOctave(int octave, int color) {
		switch (octave) {
			case 1:
				((Button) keysFragment.getView().findViewById(R.id.btnOctave1)).setTextColor(color);			
				break;
			case 2:
				((Button) keysFragment.getView().findViewById(R.id.btnOctave2)).setTextColor(color);			
				break;
			case 3:
				((Button) keysFragment.getView().findViewById(R.id.btnOctave3)).setTextColor(color);			
				break;
			case 4:
				((Button) keysFragment.getView().findViewById(R.id.btnOctave4)).setTextColor(color);			
				break;
			case 5:
				((Button) keysFragment.getView().findViewById(R.id.btnOctave5)).setTextColor(color);			
				break;
			default:
				break;
		}
	}
	
	/**
	 * Change octave from last one to new value.
	 * @param old octave number
	 * @param new octave number
	 */
	private void changeOctave(int octaveOld, int octaveNew ) {
		setTextColorBtnOctave(octaveOld, Color.BLACK);
		setTextColorBtnOctave(octaveNew, Color.WHITE);
	}
	//================================================================================
    // Service broadcast receiver methods
    //================================================================================

	public void onConnect() {
		 Log.d("BROADCAST"," onRecieve connected"); 
		 this.app.setConnected(true);
		 if(keysFragment.btnConnected!=null){
			 //set state connected on toggle button
			 ((ToggleButton) keysFragment.getView().findViewById(R.id.btnConnected)).setChecked(true);
		 }
	}

	public void onDisconnect() {
		  Log.d("BROADCAST"," onRecieve disconnected"); 
		  this.app.setConnected(false);
		  
		  if(keysFragment!=null){
			  //set state disconnected on toggle button
			  ((ToggleButton) keysFragment.getView().findViewById(R.id.btnConnected)).setChecked(false);
		  }
	}

	//================================================================================
    // Fragment Callback Methods
    //================================================================================

	@Override
	public void onKeyboardPressed() {
		if(keysFragment == null){
			keysFragment = new KeysFragment();		
		}		
		 FragmentManager fragmentManager = getFragmentManager();
	     FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	     fragmentTransaction.replace(R.id.fragment_container, keysFragment, KeysFragment.TAG);
	     fragmentTransaction.addToBackStack(null);
	     fragmentTransaction.commit();		
	}	 
	
	@Override
	public void onBackPressed(){
		if(splashFragment == null){
			splashFragment = new SplashFragment();		
		}		
		 FragmentManager fragmentManager = getFragmentManager();
	     FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	     fragmentTransaction.replace(R.id.fragment_container, splashFragment, SplashFragment.TAG);
	     fragmentTransaction.addToBackStack(null);
	     fragmentTransaction.commit();		  
	}
	
	
	@Override
	public void onSettingsPressed(){
		if(settingsFragment == null){
			settingsFragment = new SettingsFragment();		
		}		
		 FragmentManager fragmentManager = getFragmentManager();
	     FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	     fragmentTransaction.replace(R.id.fragment_container, settingsFragment, SettingsFragment.TAG);
	     fragmentTransaction.addToBackStack(null);
	     fragmentTransaction.commit();	
	
	}
	@Override
	public void onExitPressed(){
		//call disconnect
		app.getBridge().disconnect();
		
		//request exit
		 Log.d("App"," Im shuting down the app!"); 

		//kill the app
		 super.finish();
	}

	@Override
	public void onConnectedPressed() {

		if(app.isConnected()){
			((ToggleButton) keysFragment.getView().findViewById(R.id.btnConnected)).setChecked(true);
			app.getBridge().disconnect();
		}else{
			Context c =  (Context) this;
	    	app.getBridge().connect(c,app.getService());  
	    	((ToggleButton) keysFragment.getView().findViewById(R.id.btnConnected)).setChecked(false);
		}
	}

	//OCTAVE buttons
	@Override
	public void onOctaveIPressed() {
		changeOctave(app.getOctave(), 1);	
		app.setOctave(1);
	}

	@Override
	public void onOctaveIIPressed() {
		changeOctave(app.getOctave(), 2);	
		app.setOctave(2);	
	}

	@Override
	public void onOctaveIIIPressed() {
		changeOctave(app.getOctave(), 3);	
		app.setOctave(3);
	}

	@Override
	public void onOctaveIVPressed() {
		changeOctave(app.getOctave(), 4);	
		app.setOctave(4);
	}

	@Override
	public void onOctaveVPressed() {
		changeOctave(app.getOctave(), 5);	
		app.setOctave(5);
	}

	@Override
	public void onNotePressed(int note, boolean down) {
			calculateNote(note, down);
	}

	private void calculateNote(int i, boolean down) {
		int note = helper.calculateOctaveTranspose(i, app.getOctave());
		if(down){
			this.app.getOut().sendOn(note, 120, app.getChannel());
		}else{
			this.app.getOut().sendOff(note, 120, app.getChannel());
		}		
	}

	@Override
	public void onPlayPressed() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.SYSTEM_EXCLUSIVE);
		byte data[] = { (byte) 0xF0, 0x7F, 0x7F,0x06, 0x02,  (byte)0xF7 };		
		try {
			m.setChannel(1);
			m.setSysExBytes(data);
			app.getOut().send(m);
		} catch (MidiMessageException e) {
			
			e.printStackTrace();
		}
		
	}

	@Override
	public void onPausePressed() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.SYSTEM_EXCLUSIVE);
		byte data[] = { (byte) 0xF0, 0x7F, 0x7F,0x06, 0x09,  (byte)0xF7 };		
		try {
			m.setChannel(1);
			m.setSysExBytes(data);
			app.getOut().send(m);
		} catch (MidiMessageException e) {
			
			e.printStackTrace();
		}
	}

	@Override
	public void onRecordPressed() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.SYSTEM_EXCLUSIVE);
		byte data[] = { (byte) 0xF0, 0x7F, 0x7F,0x06, 0x06,  (byte)0xF7 };		
		try {
			m.setChannel(1);
			m.setSysExBytes(data);
			app.getOut().send(m);
		} catch (MidiMessageException e) {
			
			e.printStackTrace();
		}
	}

	@Override
	public void onStopPressed() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.SYSTEM_EXCLUSIVE);
		byte data[] = { (byte) 0xF0, 0x7F, 0x7F,0x06, 0x01,  (byte)0xF7 };		
		try {
			m.setChannel(1);
			m.setSysExBytes(data);
			app.getOut().send(m);
		} catch (MidiMessageException e) {
			
			e.printStackTrace();
		}
	}

	@Override
	public void onNextPressed() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.SYSTEM_EXCLUSIVE);
		byte data[] = { (byte) 0xF0, 0x7F, 0x7F,0x06, 0x04,  (byte)0xF7 };		
		try {
			m.setChannel(1);
			m.setSysExBytes(data);
			app.getOut().send(m);
		} catch (MidiMessageException e) {
			
			e.printStackTrace();
		}
	}

	@Override
	public void onPreviousPressed() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.SYSTEM_EXCLUSIVE);
		byte data[] = { (byte) 0xF0, 0x7F, 0x7F,0x06, 0x05,  (byte)0xF7 };		
		try {
			m.setChannel(1);
			m.setSysExBytes(data);
			app.getOut().send(m);
		} catch (MidiMessageException e) {
			
			e.printStackTrace();
		}
	}

	@Override
	public void onPitchBendReset() {
		app.getOut().sendPitchBend(0.0f, app.getChannel());
	}

	@Override
	public void onPitchBend(float bend) {
		app.getOut().sendPitchBend(bend, app.getChannel());
		
	}

	@Override
	public void onResetPressed() {
		app.getOut().sendControlChange(120, 0, app.getChannel());
		//PANIC MODE
		for(int i = 1; i<120; i++){
			app.getOut().sendOn(i, 0, app.getChannel());
		}
		
	}

	@Override
	public void onModulationChange(int change) {
		app.getOut().sendControlChange(1, change, app.getChannel());
		
	}

	@Override
	public void onModulationReset() {
		app.getOut().sendControlChange(1, 0, app.getChannel());
		
	}

	@Override
	public void onPitchSensitivityChanged(int value) {
		app.getOut().sendControlChange(20, value, app.getChannel());
		
	}
		
	
}


