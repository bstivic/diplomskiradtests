package hr.foi.midicontroller.bridge;

import hr.foi.midicontroller.core.MidiMessage;
import hr.foi.midicontroller.service.IService;

import java.util.List;

import android.content.ComponentName;
import android.content.Context;


public interface IMidiBridge {	
	public boolean add(MidiMessage message);	
	public MidiMessage poll();
	public List<MidiMessage> pollAll();
	public void clearBuffer();
	public boolean isConnected();
	public ComponentName connect(Context context, IService service);
	public void softReset();
	public void hardReset();
	public void disconnect();
	public void pause();	
}
