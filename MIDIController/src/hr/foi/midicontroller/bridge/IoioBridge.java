package hr.foi.midicontroller.bridge;

import hr.foi.midicontroller.core.MidiMessage;
import hr.foi.midicontroller.service.IService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

public class IoioBridge implements IMidiBridge {
	public static ArrayBlockingQueue<MidiMessage> _outputBuffer;
	private Context _parentContext;
	private final int _bufferSize;
	private boolean _connected;
	private Intent _intent;
	
	//================================================================================
    // Constructors
    //================================================================================

	public IoioBridge(int bufferSize) {
		 IoioBridge._outputBuffer =  new ArrayBlockingQueue<MidiMessage>(bufferSize);
		 this._bufferSize = bufferSize;
	}
	
	
	//================================================================================
    // Properties
    //================================================================================

	public int getBufferSize() {
		return _bufferSize;
	}
	
	
	
	//================================================================================
    // Methods
    //================================================================================

	@Override
	public boolean add(MidiMessage message) {		
		try{
			if(message != null){
				_outputBuffer.add(message);
				// Log.d("App"," IoioBridge note on"); 
				return true;
			}else {
				return false;
			}		
		}catch(IllegalStateException e){
			return false;
		}
	}

	@Override
	public MidiMessage poll() {
		return  _outputBuffer.poll();
	}

	@Override
	public List<MidiMessage> pollAll() {
		List<MidiMessage> listOfMessages = new ArrayList<MidiMessage>();
		_outputBuffer.drainTo(listOfMessages);
		return listOfMessages;
	}

	@Override
	public void clearBuffer() {
		_outputBuffer.clear();		
	}
	
	public boolean isConnected() {
		if(_intent != null){
			_connected = true;
		}
		return _connected;
	}

	@Override
	public ComponentName connect(Context context, IService service) {
		try {
			if(_connected) {
				disconnect();
			}
			this._parentContext = context;
		    this._intent = new Intent(_parentContext,service.getClass());	    
		    ComponentName started = _parentContext.startService(this._intent);		
			this._connected = true;
			
			return started;
		}catch (Exception e){
			this._connected = false;
			return null;
		}
	}

	

	@Override
	public void disconnect() {
		try{
			_parentContext.stopService(_intent);
		} catch(Exception e){
			//
		}
		_connected = false;
		_intent = null;
		_parentContext = null;
		
	}

	@Override
	public void pause() {}

	@Override
	public void softReset() {}

	@Override
	public void hardReset() {}

}
