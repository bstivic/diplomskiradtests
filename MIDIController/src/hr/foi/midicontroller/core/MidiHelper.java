package hr.foi.midicontroller.core;

public class MidiHelper {
	private int lovestOctave;
	private int highestOctave;
	
	//================================================================================
    // Constructors
    //================================================================================

	public MidiHelper(){
		lovestOctave = 1;
		highestOctave = 5;
	}
	
	
	//================================================================================
    // Properties
    //================================================================================

	public int getLovestOctave() {
		return lovestOctave;
	}

	public void setLovestOctave(int lovestOctave) {
		this.lovestOctave = lovestOctave;
	}

	public int getHighestOctave() {
		return highestOctave;
	}

	public void setHighestOctave(int highestOctave) {
		this.highestOctave = highestOctave;
	}
	
	
	//================================================================================
    // Methods
    //================================================================================

	/**
	 * Keep in mind that 3 is a middle octave and C is number 60 in octave 3.
	 * @param note
	 * @param octave
	 * @return calulated note in octave
	 */
	public int calculateOctaveTranspose(int note, int octave){
		if(octave < highestOctave){
			return note - (4-lovestOctave)*12 + octave*12;
		}else{
			return note - (4-lovestOctave)*12 + highestOctave*12;
		}
	}
	

}
