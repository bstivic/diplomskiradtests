package hr.foi.midicontroller.core;

import hr.foi.midicontroller.exceptions.MidiMessageException;

public class MidiMessage {
	private MidiType _messageType;
	private byte _byte1;
	private byte _byte2;
	private byte[] _sysExBytes; 
	private boolean _valid = true;
	private byte[] _hexValue;
	private int _channel;
	
	//================================================================================
    // Enumerations
    //================================================================================

	public enum MidiType {
		INVALID_TYPE(0x00), 		//For notifying errors.								= 0x00,0
		NOTE_ON (0x90),				//Note On. 											= 0x90,144
		NOTE_OFF(0x80),				//Note Off. 										= 0x80,128
		AFTERTOUCH_POLY(0xA0),		//Polyphonic AfterTouch. 							= 0xA0,160
		CONTROL_CHANGE(0xB0),		//Control Change / Channel Mode. 					= 0xB0,176
		PROGRAM_CHANGE(0xC0),		//Program Change. 									= 0xC0,192
		AFTERTOUCH_CHANNEL(0xD0),	//Channel (monophonic) AfterTouch. 					= 0xD0,208
		PITCH_BEND(0xE0),			//Pitch Bend. 										= 0xE0,224
		SYSTEM_EXCLUSIVE(0xF0),		//System Exclusive. 								= 0xF0,240
		TIMECODE_QUARTERFRAME(0xF1),//System Common - MIDI Time Code Quarter Frame. 	= 0xF1,241
		SONG_POSITION(0xF2),		//System Common - Song Position Pointer. 			= 0xF2,242
		SONG_SELECT(0xF3),			//System Common - Song Select. 						= 0xF3,243
		TUNE_REQUEST(0xF6),			//System Common - Tune Request. 					= 0xF6,246
		TIMING_CLOCK(0xF8),			//System Real Time - Timing Clock. 					= 0xF8,248
		START(0xFA),				//System Real Time - Start.							= 0xFA,250
		CONTINUE(0xFB),				//System Real Time - Continue.						= 0xFB,251
		STOP(0xFC),					//System Real Time - Stop.							= 0xFC,252
		ACTIVE_SENSING(0xFE),		//System Real Time - Active Sensing. 				= 0xFE,254
		SYSTEM_RESET(0xFF);			//System Real Time - System Reset. 					= 0xFF,255
				
		private int value;

        private MidiType(int value){
        	this.value = value;
        }
        
        public int getValue(){
        	return this.value;
        }
	};   
	
	//================================================================================
    // Constructors
    //================================================================================

	public MidiMessage() {}
	
	/***
	 * Constructor for message with one data byte.
	 * @param Midi message type
	 * @param Data byte 1
	 * @throws MidiMessageException
	 */
	public MidiMessage(MidiType type, int data1) throws MidiMessageException{
		this.setByte1(data1);
		this._messageType = type;
	}
	
	/***
	 * Constructor for messages with two data bytes.
	 * @param Midi message type
	 * @param Data byte 1
	 * @param Data byte 2
	 * @throws MidiMessageException
	 */
	public MidiMessage(MidiType type, int data1, int data2) throws MidiMessageException{
		this.setByte1(data1);
		this.setByte2(data2);
		this._messageType = type;
	}
	
	/***
	 * Constructor for messages with two data bytes and channel.
	 * @param Midi message type
	 * @param Data byte 1
	 * @param Data byte 2
	 * @param Midi Channel 1-16 
	 * @throws MidiMessageException
	 */
	public MidiMessage(MidiType type, int data1, int data2, int channel) throws MidiMessageException{
		this.setByte1(data1);
		this.setByte2(data2);
		this._messageType = type;
		this.setChannel(channel);
	}
	
	//================================================================================
    // Properties
    //================================================================================

	public void setByte1(int byte1) throws MidiMessageException {
		byteDataInRange(byte1);
		this._byte1 = (byte) byte1;
	}
	
	public byte getByte1() {
		return _byte1;
	}
	
	public void setByte2(int byte2) throws MidiMessageException{
		byteDataInRange(byte2);
		this._byte2 = (byte)byte2;
	}
	
	public int getByte2() {
		return this._byte2;
	}
	
	public void setSysExBytes(byte[] bytesData) throws MidiMessageException {
		if(bytesData == null) {
			_valid = false;
			throw new MidiMessageException("SysExBytes Data is null!");
		}
		_sysExBytes = bytesData;
	}
	
	public byte[] getSysExBytes() {
		return _sysExBytes;
	}
	
	public void setMessageType(MidiType midiType) {
		this._messageType = midiType;
	}
	public MidiType getMessageType() {
		return _messageType;
	}
	 
	public boolean isValid() {
		//test if data is empty
		if(this._messageType == null) {
			this._valid = false;
		}
		return _valid;
	}
	
	public void setChannel(int channel){
		this._channel = channel;
	}
	
	public int getChannel() {
		return this._channel;
	}
	
	public byte[] getHexValue() {
		
		return _hexValue;
	}
		
	//================================================================================
    // Methods
    //================================================================================
	
	/***
	 * Checks if Midi byte data 1 and data 2 are in given range of values 0-127
	 * @param byte1
	 * @throws MidiMessageException
	 */
	private void byteDataInRange(int byte1) throws MidiMessageException {
		if (byte1 > 127){
			_valid = false;
			throw new MidiMessageException("Byte data is larger than expected value of 127!");
		}else if (byte1 < 0) {
			_valid = false;
			throw new MidiMessageException("Byte data is smaller than expected value of 0!");
		}
	}
	
	/***
	 * Method for calculation byte array of MidiMessage object.
	 * @return byte array of real data for sending over wire
	 */
	public byte[] calculateMessageToByteArray() {
		if(this.isValid()) {		
			int sizeOfMessage = this.calculateSize();		
			byte[] messageHex = createMessageArray(sizeOfMessage);	
			//validate channel
			if(this._channel >= 1 && this._channel <= 16) {
				//add channel to midi type
				messageHex[0] = mergeChannelAndType(this._channel, this._messageType.value);
				
				//check data length 				
				if(sizeOfMessage == 1){
					messageHex[1] =  removeMSB(this._byte1);
				}else if(sizeOfMessage == 2){
					messageHex[1] =  removeMSB(this._byte1);
					messageHex[2] =  removeMSB(this._byte2);
				}else if(sizeOfMessage > 0) {
					//copy SysEscl byte array to new array
					for(int i = 0; i < sizeOfMessage; i++){
						messageHex[i] = this._sysExBytes[i];
					}					
				}					
				//save hex value of message for performance fix
				this._hexValue = messageHex;
				
				return messageHex;
			}else {
				return  null;
			}		
		}else {
			return null;
		}
	}

	private byte removeMSB(byte data) {
		byte removed = (byte) (data & 0x7f);
		return removed;
	}

	private byte mergeChannelAndType(int channel, int type) {
		return (byte) (type | (((byte)(channel- 1)) & 0x0f));
	}

	private byte[] createMessageArray(int sizeOfMessage) {
		byte[] messageHex;
		switch(sizeOfMessage) {
			case 1:
				messageHex = new byte [2];
				break;
			case 3:
				messageHex = new byte[3];
				break;
			case 2:
				messageHex = new byte[3];
				break;
			default: 
				messageHex = new byte[sizeOfMessage+1];
				break;			
		}
		return messageHex;
	}
	
	/**
	 * Calculates size of data byte packages for MidiMessage. 
	 * @return
	 */
	private int calculateSize() {
		switch(_messageType) {
		case SYSTEM_EXCLUSIVE: 
			return _sysExBytes.length;
		case PROGRAM_CHANGE:
			return 1;
		case AFTERTOUCH_CHANNEL:
			return 1;
		case TIMECODE_QUARTERFRAME:
			return 1;
		case TUNE_REQUEST:
			return 0;
		case TIMING_CLOCK:
			return 0;
		case START:
			return 0;
		case CONTINUE:
			return 0;
		case STOP:
			return 0;
		case ACTIVE_SENSING:
			return 0;
		case SYSTEM_RESET:
			return 0;
		case SONG_SELECT:
			return 1;
		default: 
			return 2;
			
		}
	}
	
}







































