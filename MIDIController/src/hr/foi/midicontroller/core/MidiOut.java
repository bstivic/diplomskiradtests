package hr.foi.midicontroller.core;

import android.util.Log;
import hr.foi.midicontroller.bridge.IMidiBridge;
import hr.foi.midicontroller.core.MidiMessage.MidiType;
import hr.foi.midicontroller.exceptions.MidiMessageException;

public class MidiOut {
	MidiMessage _lastMessage; 
	IMidiBridge _midiBridge;
	
	//================================================================================
    // Constructors
    //================================================================================
	public MidiOut(IMidiBridge bridge) {
		this._midiBridge = bridge;
	}
	
	//================================================================================
    // Properties
    //================================================================================
	public MidiMessage getLastMessage() {		
		return  this._lastMessage;
	}
	
	
	//================================================================================
    // Methods
    //================================================================================

	public boolean send(MidiMessage message) {
		byte[] hexValue =  message.calculateMessageToByteArray();
		
		if(hexValue != null) {
			boolean sent = _midiBridge.add(message);
			this._lastMessage = message;
			Log.d("App", hexValue.toString());
			return sent;
		}else {
			return false;
		}
	}  
	public boolean sendOn(int note, int velocity, int channel) {
		try {
			MidiMessage m = new MidiMessage(MidiType.NOTE_ON, note, velocity, channel);
			 Log.d("App"," Note ON- MidiOut"+note); 
			return send(m);
		} catch (MidiMessageException e) {
			return false;
		}		
	}
	
	public boolean sendOff(int note, int velocity, int channel) {
		try {
			MidiMessage m = new MidiMessage(MidiType.NOTE_OFF, note, velocity, channel);
			 Log.d("App"," Note off- MidiOut "+note); 
			return send(m);
		} catch (MidiMessageException e) {
			return false;
		}	
	}
	
	public boolean sendAfterTouchPoly(int note, int velocity, int channel) {
		try {
			MidiMessage m = new MidiMessage(MidiType.AFTERTOUCH_POLY, note, velocity, channel);
			return send(m);
		} catch (MidiMessageException e) {
			return false;
		}	
	}
	
	public boolean sendControlChange(int controller, int value, int channel) {
		try {
			MidiMessage m = new MidiMessage(MidiType.CONTROL_CHANGE, controller, value, channel);
			return send(m);
		} catch (MidiMessageException e) {
			return false;
		}	
	}
	
	public boolean sendProgramChange(int newProgram, int channel) {
		try {
			MidiMessage m = new MidiMessage();
			m.setMessageType(MidiType.PROGRAM_CHANGE);
			m.setByte1(newProgram);
			m.setChannel(channel);
			
			return send(m);
		} catch (MidiMessageException e) {
			return false;
		}	
	}
	
	public boolean sendAfterTouchChannel(int greatestPressureValue, int channel) {
		try {
			MidiMessage m = new MidiMessage();
			m.setMessageType(MidiType.AFTERTOUCH_CHANNEL);
			m.setByte1(greatestPressureValue);
			m.setChannel(channel);
			return send(m);
		} catch (MidiMessageException e) {
			return false;
		}	
	}
	
	/***
	 * Send pitch bend message controled with float number between -1.0f and 1.0f.
	 * @param pitchValue -1.0f <- 0.0f -> 1.0f
	 * @param channel
	 * @return message sent true/false
	 */
	public boolean sendPitchBend(float pitchValue, int channel) {
		try {
			if(pitchValue > 1.0f || pitchValue < -1.0f) {
				return false;
			}
			
			int value = (int) ((pitchValue+1.0f)*8192);
		    if (value > 16383) value = 16383; 
			
			MidiMessage m = new MidiMessage();
			m.setMessageType(MidiType.PITCH_BEND);
			m.setByte1((value & 0x7F));
			m.setByte2((value >> 7) & 0x7F);
			m.setChannel(channel);
			
			return send(m);
		} catch (MidiMessageException e) {
			return false;
		}	
	}
	
	public boolean sendTimeCodeQuarterFrame(int data) {
		try {
			MidiMessage m = new MidiMessage();
			m.setMessageType(MidiType.TIMECODE_QUARTERFRAME);
			m.setByte1(data);
			m.setChannel(2);
			return send(m);
		} catch (MidiMessageException e) {
			return false;
		}	
	}
	
	public boolean sendSongPosition(int data1, int data2) {
		try {
			MidiMessage m = new MidiMessage();
			m.setMessageType(MidiType.SONG_POSITION);
			m.setByte1(data1);
			m.setByte2(data2);
			m.setChannel(3);
			return send(m);
		} catch (MidiMessageException e) {
			return false;
		}	
	}
	
	public boolean sendSongSelect(int data) {
		try {
			MidiMessage m = new MidiMessage();
			m.setMessageType(MidiType.SONG_SELECT);
			m.setByte1(data);
			m.setChannel(4);
			return send(m);
		} catch (MidiMessageException e) {
			return false;
		}	
	}
	
	public boolean sendTuneRequest() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.TUNE_REQUEST);
		m.setChannel(7);
		return send(m);	
	}
	
	public boolean sendTimingClock() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.TIMING_CLOCK);
		m.setChannel(9);
		return send(m);	
	}
	
	public boolean sendStart() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.START);
		m.setChannel(11);
		
		return send(m);	
	}
	public boolean sendContinue() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.CONTINUE);
		m.setChannel(12);

		return send(m);	
	}
	public boolean sendStop() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.STOP);
		m.setChannel(13);
		return send(m);	
	}
	public boolean sendActiveSensing() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.ACTIVE_SENSING);
		m.setChannel(15);

		return send(m);	
	}
	public boolean sendSystemReset() {
		MidiMessage m = new MidiMessage();
		m.setMessageType(MidiType.SYSTEM_RESET);
		m.setChannel(16);

		return send(m);	
	}
}































