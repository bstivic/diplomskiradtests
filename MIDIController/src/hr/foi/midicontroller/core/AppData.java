package hr.foi.midicontroller.core;

import hr.foi.midicontroller.bridge.IMidiBridge;
import hr.foi.midicontroller.core.MidiMessage.MidiType;
import hr.foi.midicontroller.exceptions.MidiMessageException;
import hr.foi.midicontroller.service.IService;


public class AppData {
	private IMidiBridge bridge;
	private MidiHelper helper;
	private IService service;
	private MidiOut out;
	private Boolean	connected;
	private int octave;
	private int channel;
	
	
	//Pitch control sensitivity and threshold
	private float sensitivityFactor = 100f;
	private float sensitivityTreshold = 0.1f;
	private float sensitivityModulationFactor = 300f;
	private float sensitivityModulationTreshold = 0.1f;
	
	//control button message 
	private MidiMessage playMessage;
	private MidiMessage pauseMessage;
	private MidiMessage stopMessage;
	private MidiMessage recordMessage;
	private MidiMessage nextMessage;
	private MidiMessage prevMessage;
	
	//================================================================================
    // Constructors
    //================================================================================

	public AppData() {
		
	}
	
	public AppData(IMidiBridge bridge, IService service, MidiOut out){
		this.service = service;
		this.bridge = bridge;
		this.out = out;
		this.connected = false;
		
		try {
			this.setPlayMessage(new MidiMessage(MidiType.START, 127, 0, 1));
			this.setPauseMessage(new MidiMessage(MidiType.STOP, 127, 0, 1));
			this.setStopMessage(new MidiMessage(MidiType.STOP, 127, 0, 1));
			this.setRecordMessage(new MidiMessage(MidiType.START, 127, 0, 1));
			this.setNextMessage(new MidiMessage(MidiType.START, 127, 0, 1));
			this.setPrevMessage(new MidiMessage(MidiType.STOP, 127, 0, 1));
		} catch (MidiMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//================================================================================
    // Properties
    //================================================================================
	public void setBridge(IMidiBridge ioio) {
		this.bridge = ioio;
	}
	public void setOut(MidiOut out) {
		this.out = out;
	}
	public void setService(IService service) {
		this.service = service;
	}
	public IMidiBridge getBridge() {
		return bridge;
	}
	public MidiOut getOut() {
		return out;
	}
	public IService getService() {
		return service;
	}
	
	public Boolean isConnected(){
		return connected;
	}
	public void setConnected(Boolean con){
		this.connected = con;
	}

	public void setOctave(int octave){
		this.octave = octave;
	}
	public int getOctave(){
		return this.octave;
	}
	public void setChannel(int channel){
		this.channel = channel;
	}
	public int getChannel(){
		return channel;
	}

	public MidiMessage getPlayMessage() {
		return playMessage;
	}

	public void setPlayMessage(MidiMessage playMessage) {
		this.playMessage = playMessage;
	}

	public MidiMessage getPauseMessage() {
		return pauseMessage;
	}

	public void setPauseMessage(MidiMessage pauseMessage) {
		this.pauseMessage = pauseMessage;
	}

	public MidiMessage getRecordMessage() {
		return recordMessage;
	}

	public void setRecordMessage(MidiMessage recordMessage) {
		this.recordMessage = recordMessage;
	}

	public MidiMessage getStopMessage() {
		return stopMessage;
	}

	public void setStopMessage(MidiMessage stopMessage) {
		this.stopMessage = stopMessage;
	}

	public MidiMessage getNextMessage() {
		return nextMessage;
	}

	public void setNextMessage(MidiMessage nextMessage) {
		this.nextMessage = nextMessage;
	}

	public MidiMessage getPrevMessage() {
		return prevMessage;
	}

	public void setPrevMessage(MidiMessage prevMessage) {
		this.prevMessage = prevMessage;
	}

	public float getSensitivityFactor() {
		return sensitivityFactor;
	}

	public void setSensitivityFactor(float sensitivityFactor) {
		this.sensitivityFactor = 3*sensitivityFactor;
	}

	public float getSensitivityTreshold() {
		return sensitivityTreshold;
	}

	public void setSensitivityTreshold(float sensitivityTreshold) {
		this.sensitivityTreshold = sensitivityTreshold;
	}

	public MidiHelper getHelper() {
		return helper;
	}

	public void setHelper(MidiHelper helper) {
		this.helper = helper;
	}

	public float getSensitivityModulationFactor() {
		return sensitivityModulationFactor;
	}

	public void setSensitivityModulationFactor(float sensitivityModulationFactor) {
		this.sensitivityModulationFactor = sensitivityModulationFactor;
	}

	public float getSensitivityModulationTreshold() {
		return sensitivityModulationTreshold;
	}

	public void setSensitivityModulationTreshold(
			float sensitivityModulationTreshold) {
		this.sensitivityModulationTreshold = sensitivityModulationTreshold;
	}
	
	//================================================================================
    // Methods
    //================================================================================

}
