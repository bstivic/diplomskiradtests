package hr.foi.midicontroller.bridge;

import java.util.ArrayList;
import java.util.List;

import hr.foi.midicontroller.core.MidiMessage;
import hr.foi.midicontroller.core.MidiMessage.MidiType;
import hr.foi.midicontroller.exceptions.MidiMessageException;

import org.robolectric.RobolectricTestRunner; 
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
public class IoioBridgeTests {
	 
	 
	@Test
	public void add_validMessage_true() throws MidiMessageException {
		IMidiBridge ioio = new IoioBridge(1);
		MidiMessage m = new MidiMessage(MidiType.NOTE_ON,60, 127, 5);
		m.calculateMessageToByteArray();		
		assertTrue(ioio.add(m));
	}
	
	@Test
	public void add_validMessageToBufferNoteC_sameHexValue943c7f() throws MidiMessageException {
		IMidiBridge ioio = new IoioBridge(100);
		MidiMessage m = new MidiMessage(MidiType.NOTE_ON,60, 127, 5);
		m.calculateMessageToByteArray();
		ioio.add(m);
		 
		assertArrayEquals(new byte[] { (byte)0x94, 0x3c, 0x7f},ioio.poll().getHexValue());
	}
	
	@Test
	public void add_validTwoMessageToBuffer_firstOne() throws MidiMessageException {
		IMidiBridge ioio = new IoioBridge(100);
		MidiMessage m = new MidiMessage(MidiType.NOTE_ON,60, 127, 5);
		m.calculateMessageToByteArray();
		ioio.add(m);
		
		MidiMessage m2 = new MidiMessage(MidiType.NOTE_ON,12, 0, 5);
		m2.calculateMessageToByteArray();
		ioio.add(m2);
		
		assertArrayEquals(new byte[] { (byte)0x94, 0x3c, 0x7f},ioio.poll().getHexValue());
	}
	
	@Test
	public void add_bufferOverflow_false() throws MidiMessageException {
		IMidiBridge ioio = new IoioBridge(1);
		MidiMessage m = new MidiMessage(MidiType.NOTE_ON,60, 127, 5);
		m.calculateMessageToByteArray();
		ioio.add(m);
		
		MidiMessage m2 = new MidiMessage(MidiType.NOTE_ON,12, 0, 5);
		m2.calculateMessageToByteArray();
		assertEquals(false,ioio.add(m2));
	}
	
	@Test
	public void add_nullObject_false() {
		IMidiBridge ioio = new IoioBridge(1);
		assertFalse(ioio.add(null));
	}	
	
	@Test
	public void poll_emptyBuffer_null() {
		IMidiBridge ioio = new IoioBridge(5);
		assertNull(ioio.poll());
	}
	
	@Test
	public void poll_oneMessageInBuffer_validMessage() throws MidiMessageException {
		IMidiBridge ioio = new IoioBridge(5);
		MidiMessage m = new MidiMessage(MidiType.NOTE_ON,60, 127, 5);
		m.calculateMessageToByteArray();
		ioio.add(m);
				
		assertEquals(m,ioio.poll());
	}
	
	@Test
	public void poll_threeMessInBuffer_firstSent() throws MidiMessageException {
		IMidiBridge ioio = new IoioBridge(5);
		MidiMessage m = new MidiMessage(MidiType.NOTE_ON,60, 127, 5);
		ioio.add(m);
		
		MidiMessage m2 = new MidiMessage(MidiType.NOTE_ON,80, 127, 1);
		ioio.add(m2);
		
		MidiMessage m3 = new MidiMessage(MidiType.NOTE_ON,90, 127, 2);
		ioio.add(m3);
				
		assertEquals(m,ioio.poll());
	}
	
	@Test
	public void poll_threeMessInBuffer_lastSent() throws MidiMessageException {
		IMidiBridge ioio = new IoioBridge(5);
		MidiMessage m = new MidiMessage(MidiType.NOTE_ON,60, 127, 5);
		ioio.add(m);
		
		MidiMessage m2 = new MidiMessage(MidiType.NOTE_ON,80, 127, 1);
		ioio.add(m2);
		
		MidiMessage m3 = new MidiMessage(MidiType.NOTE_ON,90, 127, 2);
		ioio.add(m3);
		
		ioio.poll();
		ioio.poll();
		
		assertEquals(m3,ioio.poll());
	}
	
	
	@Test
	public void pollAll_oneMessageInBuffer_sentMessage() throws MidiMessageException{
		IMidiBridge ioio = new IoioBridge(5);
		MidiMessage m = new MidiMessage(MidiType.NOTE_ON,60, 127, 5);
		ioio.add(m);
		List<MidiMessage> listMessages =  new ArrayList<MidiMessage>();
		listMessages.add(m);
		assertTrue("Polled list is not equal!", listMessages.equals(ioio.pollAll()));	
	}
	
	@Test
	public void pollAll_emptyBuffer_emptyList() throws MidiMessageException{
		IMidiBridge ioio = new IoioBridge(5);
		List<MidiMessage> listMessages =  new ArrayList<MidiMessage>();
		assertTrue("Polled list is not equal!", listMessages.equals(ioio.pollAll()));	
	}
		
	@Test
	public void pollAll_twoMessageInBuffer_bothMessages() throws MidiMessageException{
		IMidiBridge ioio = new IoioBridge(5);
		MidiMessage m = new MidiMessage(MidiType.NOTE_ON,60, 127, 5);
		MidiMessage m2= new MidiMessage(MidiType.NOTE_ON,65, 127, 1);
		ioio.add(m);
		ioio.add(m2);
		List<MidiMessage> listMessages =  new ArrayList<MidiMessage>();
		listMessages.add(m);
		listMessages.add(m2);
		assertEquals(listMessages,ioio.pollAll());
	}
	
	@Test
	public void clearBuffer_emptyBuffer_emptyBuffer() {
		IMidiBridge ioio = new IoioBridge(5);
		ioio.clearBuffer();
		assertEquals(null,ioio.poll());
	}
	
	@Test
	public void clearBuffer_fullBuffer_emptyBuffer() throws MidiMessageException {
		IMidiBridge ioio = new IoioBridge(1);
		MidiMessage m = new MidiMessage(MidiType.NOTE_ON,60, 127, 5);
		ioio.add(m);
		ioio.clearBuffer();
		assertEquals(null,ioio.poll());
	}
	
	
}
