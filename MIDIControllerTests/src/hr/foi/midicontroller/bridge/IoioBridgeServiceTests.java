package hr.foi.midicontroller.bridge;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import hr.foi.midicontroller.service.IService;
import hr.foi.midicontroller.stubs.IoioOutServiceStub;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import android.content.Context;

@RunWith(RobolectricTestRunner.class)
public class IoioBridgeServiceTests {
	private IMidiBridge ioio;
	private Context context;
	private IService service;
	
    @Before
	public void setup() throws Exception{
    	this.ioio = new IoioBridge(1);
    	this.context = Robolectric.getShadowApplication().getApplicationContext();
    	this.service = new IoioOutServiceStub();        	
    }
    
	@After
	public void teardown(){
		ioio.disconnect();
	}
	
	@Test
	public void connect_validContext_true() {
		ioio.connect(context,service);	
		assertTrue(ioio.isConnected());
	}
 
	@Test
	public void connect_invalidContext_null() {			
		assertNull(ioio.connect(null,service));			
	}

	@Test
	public void isConnected_invalidContext_false() {
		ioio.connect(null,service);
		assertFalse(ioio.isConnected());	
	}
	
	@Test
	public void disconnect_notStartedService_isConnectedIsFalse() {
		ioio.connect(context,service);
		ioio.disconnect();
		assertFalse(ioio.isConnected());	
	}
	
	@Test
	public void disconnect_startedService_isConnectedIsFalse() {
		ioio.connect(context,service);
		ioio.disconnect();
		assertFalse(ioio.isConnected());	
	}
	
	@Test
	public void connect_startedService_isConnectedIsTrue() {
		ioio.connect(context,service);
		ioio.connect(context,service);
		assertTrue(ioio.isConnected());	
	}
	


	

}
