package hr.foi.midicontroller.mocks;

import hr.foi.midicontroller.bridge.IMidiBridge;
import hr.foi.midicontroller.core.MidiMessage;
import hr.foi.midicontroller.service.IService;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import android.content.ComponentName;
import android.content.Context;

public class MidiBridgeMock implements IMidiBridge{
	public final ArrayBlockingQueue<MidiMessage> _outputBuffer;
	public boolean connected;
	
	
	public MidiBridgeMock(int bufferSize) {
			 this._outputBuffer =  new ArrayBlockingQueue<MidiMessage>(bufferSize);
	}
	
	@Override
	public boolean add(MidiMessage message) {		
		return _outputBuffer.add(message);
	}

	@Override
	public MidiMessage poll() {
		return  _outputBuffer.poll();
	}

	@Override
	public List<MidiMessage> pollAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clearBuffer() {
		_outputBuffer.clear();
	}

	@Override
	public boolean isConnected() {
		return connected;
	}

	@Override
	public void softReset() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hardReset() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnect() {
		this.connected = false;
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public ComponentName connect(Context context, IService service) {
		this.connected = true;
		service.sendBroadcastMessage("midi.board.CONNECTED");
		return null;
	}

}
