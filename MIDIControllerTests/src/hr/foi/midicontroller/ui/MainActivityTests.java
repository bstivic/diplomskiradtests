package hr.foi.midicontroller.ui;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import hr.foi.midicontroller.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import android.app.Fragment;
import android.view.View;

 

@RunWith(RobolectricTestRunner.class)
public class MainActivityTests {
	private MainActivity activity;
	
	@Before
	public void setup() throws Exception{
		 activity = Robolectric.buildActivity( MainActivity.class )
                 .create()
                 .get();
	}
		  
	@Test
	public void activity_isNotNull(){
		assertNotNull( activity );
	}
	
	@Test
	public void onCreate_initializeBridge_notNull(){
		assertNotNull(activity.app.getBridge());
	}
	
	@Test 
	public void onCreate_initializeMidiOut_notNull(){
		assertNotNull(activity.app.getOut());		
	}
	
	@Test 
	public void onCreate_initializeService_notNull(){
		assertNotNull(activity.app.getService());		
	}
	
	@Test
	public void onCreate_startingFragmetSplash_notNull(){
		assertNotNull( activity.getFragmentManager().findFragmentByTag(SplashFragment.TAG));
	}
	
	@Test
	public void onCreate_startingFragmetSplash_visible(){
		Fragment fragment =  activity.getFragmentManager().findFragmentByTag(SplashFragment.TAG);
		View splashView = fragment.getView().findViewById(R.id.splash_fragment);
		assertEquals(splashView.getVisibility(), View.VISIBLE);
	}
	


}
