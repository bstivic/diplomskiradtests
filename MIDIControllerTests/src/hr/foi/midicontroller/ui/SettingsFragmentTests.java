package hr.foi.midicontroller.ui;


import static org.junit.Assert.*;
import hr.foi.midicontroller.R;
import hr.foi.midicontroller.bridge.IMidiBridge;
import hr.foi.midicontroller.mocks.MidiBridgeMock;
import hr.foi.midicontroller.service.IService;
import hr.foi.midicontroller.stubs.IoioOutServiceStub;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.util.ActivityController;

import android.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;


@RunWith(RobolectricTestRunner.class)
public class SettingsFragmentTests {
	
	private MainActivity activity;
	private Fragment fragment;
	private View splashView;
	
	IMidiBridge bridge;
	IService service;
	ActivityController<MainActivity> controller;
	
	//fragments
	Fragment splashFragment;
	Fragment settingsFragment;
	
	//views
	 View settingsView;
	
	@Before
	public void setup() throws Exception{
		 controller = Robolectric.buildActivity( MainActivity.class).create();
		 activity =  (MainActivity)controller.get();
		 
		 //settings mocks and stubs
		 bridge = new MidiBridgeMock(100);
		 service = new IoioOutServiceStub();
		 activity.app.setBridge(bridge);
		 activity.app.setService(service);
		 
		 this.splashFragment =  activity.getFragmentManager().findFragmentByTag(SplashFragment.TAG);
	     this.splashView = splashFragment.getView().findViewById(R.id.splash_fragment);
		 Button btnSettings = (Button) splashView.findViewById(R.id.btnSettings);
		 btnSettings.performClick();	
	     this.settingsFragment  = activity.getFragmentManager().findFragmentByTag(SettingsFragment.TAG);
	     this.settingsView = settingsFragment.getView().findViewById(R.id.settings_fragment);
	     

	}
	 
	@Test
	public void settingsFragment_isNotNull(){
		assertNotNull( activity.getFragmentManager().findFragmentByTag(SettingsFragment.TAG));
	}
	
	@Test
	public void settingsFragmet_movePithcSensitivit_value0(){
		SeekBar pitchSensitivy = (SeekBar) settingsView.findViewById(R.id.seekPitchSens);
		//pitchSensitivy.setProgress(10);
		//assertEquals(10, activity.app.getSensitivityFactor(), 0.001);
	}
	
	
	
}