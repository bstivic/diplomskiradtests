package hr.foi.midicontroller.ui;

import hr.foi.midicontroller.bridge.IMidiBridge;
import hr.foi.midicontroller.mocks.MidiBridgeMock;
import hr.foi.midicontroller.service.IService;
import hr.foi.midicontroller.stubs.IoioOutServiceStub;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.util.ActivityController;

import android.content.Intent;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
public class MainActivityServiceTests {
	private MainActivity activity;
	private ActivityController<MainActivity> controller;
	
    
    @Before
	public void setup() throws Exception{
     	this.controller = Robolectric.buildActivity( MainActivity.class).create();
    	this.activity =  (MainActivity)controller.get();
    	
    	IMidiBridge bridge = new MidiBridgeMock(100);
		IService service = new IoioOutServiceStub();
		activity.app.setBridge(bridge);
		activity.app.setService(service);
    } 
    
	/***
	 * Test for registerReceiver(mReceiverConnected, mIntentFilterConnected);
	 * Not connected
	 * 
	 */
	@Test
	public void mReceiverConnected_isConnected_false()
	{
		assertFalse(activity.app.isConnected());
	}
		
	/***
	 * Test for registerReceiver(mReceiverConnected, mIntentFilterConnected);
	 * IntentFilter("midi.board.CONNECTED")
	 * 
	 */
	@Test
	public void onResume_mReceiverConnected_registered()
	{ 
		this.controller.resume();
		Intent intent = new Intent("midi.board.CONNECTED");
		ShadowApplication shadowApplication = Robolectric.getShadowApplication();
        assertTrue(shadowApplication.hasReceiverForIntent(intent));
       
	} 
	
	/***
	 * Test for registerReceiver(mReceiverConnected, mIntentFilterConnected);
	 * IntentFilter("midi.board.CONNECTED")
	 * 
	 */
	@Test
	public void onPause_mReceiverConnected_notRegistered() throws Exception
	{ 
		this.controller.resume().pause();
		Intent intent = new Intent("midi.board.CONNECTED");
		ShadowApplication shadowApplication = Robolectric.getShadowApplication();
		assertFalse(shadowApplication.hasReceiverForIntent(intent));			
	}
	
	/***
	 * Test for registerReceiver(mReceiverConnected, mIntentFilterConnected);
	 * IntentFilter("midi.board.CONNECTED")
	 * 
	 */
	@Test
	public void onPause_mReceiverDisconnected_notRegistered()
	{
		this.controller.resume().pause();
		Intent intent = new Intent("midi.board.DISCONNECTED");
		ShadowApplication shadowApplication = Robolectric.getShadowApplication();
        assertFalse(shadowApplication.hasReceiverForIntent(intent));
	}
		
	
	/***
	 * Test for registerReceiver(mReceiverDisconnected, mIntentFilterDisconnected);
	 * IntentFilter("midi.board.DISCONNECTED")
	 * 
	 */
	@Test
	public void onCreate_mReceiverDisconnected_notRegistered()
	{
		Intent intent = new Intent("midi.board.DISCONNECTED");
		ShadowApplication shadowApplication = Robolectric.getShadowApplication();
        assertFalse(shadowApplication.hasReceiverForIntent(intent));

	}
		
	/***
	 * Test for registerReceiver(mReceiverDisconnected, mIntentFilterDisconnected);
	 * IntentFilter("midi.board.DISCONNECTED")
	 * 
	 */
	@Test
	public void onResume_mReceiverDisconnected_registered()
	{
		this.controller.resume();
		Intent intent = new Intent("midi.board.DISCONNECTED");
		ShadowApplication shadowApplication = Robolectric.getShadowApplication();
        assertTrue(shadowApplication.hasReceiverForIntent(intent));
	}
		

	
	//*****************************************************************************
	//START/STOP SERVICE
	//*****************************************************************************
	

	@Test
	public void onCreate_bridgeConnectStartService_disconnected(){
		 activity = Robolectric.buildActivity( MainActivity.class ).create().get();
		 assertFalse(activity.app.getBridge().isConnected());
	}
	
	@Test
	public void onStart_bridgeConnectStartService_connected(){
		@SuppressWarnings("rawtypes")
		ActivityController controller = Robolectric.buildActivity( MainActivity.class ).create();
		MainActivity activity = (MainActivity) controller.get();
		activity.app.setService(new IoioOutServiceStub());
		controller.start();
		assertTrue(activity.app.getBridge().isConnected());
	}
		
	@Test
	public void onStop_bridgeConnectStartService_discconnected(){
		@SuppressWarnings("rawtypes")
		ActivityController controller = Robolectric.buildActivity( MainActivity.class ).create();
		MainActivity activity = (MainActivity) controller.get();
		activity.app.setService(new IoioOutServiceStub());
		controller.start();
		assertTrue(activity.app.getBridge().isConnected());
		controller.stop();
		assertFalse(activity.app.getBridge().isConnected());	
	}

	
}





























