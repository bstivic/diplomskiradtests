package hr.foi.midicontroller.ui;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import hr.foi.midicontroller.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import android.app.Fragment;
import android.view.View;
import android.widget.Button;


@RunWith(RobolectricTestRunner.class)
public class SplashFragmentTest {
	
private MainActivity activity;
	private Fragment fragment;
	private View splashView;
	@Before
	public void setup() throws Exception{
		 activity = Robolectric.buildActivity( MainActivity.class )
                 .create()
                 .start()
                 .resume()
                 .get();
		 fragment =  activity.getFragmentManager().findFragmentByTag(SplashFragment.TAG);
		 splashView = fragment.getView().findViewById(R.id.splash_fragment);
	}
		
	 
	@Test
	public void splashFragment_isNotNull(){
		assertNotNull( activity.getFragmentManager().findFragmentByTag(SplashFragment.TAG));
	}
	//button Keyboard settings visible
	@Test
	public void btnKeyboard_onCreate_visible(){
		Button btnKeyboard = (Button) splashView.findViewById(R.id.btnKeyboard);	
		assertEquals(btnKeyboard.getVisibility(), Button.VISIBLE);
	}
	
	//onClick keyboard shows keyboard fragment
	public void btnKeyboardOnClickListener_keysFragment_visible(){
		Button btnKeyboard = (Button) splashView.findViewById(R.id.btnKeyboard);
		btnKeyboard.performClick();	
				
		Fragment fragmentKeys =  activity.getFragmentManager().findFragmentByTag(KeysFragment.TAG);
		View keysView = fragmentKeys.getView().findViewById(R.id.keys_fragment);	
		
		assertEquals(keysView.getVisibility(), View.VISIBLE);
	}
	
	
	//btnSettings visible
	@Test
	public void btnSettings_onCreate_visible(){
		Button btnSettings = (Button) splashView.findViewById(R.id.btnSettings);	
		assertEquals(btnSettings.getVisibility(), Button.VISIBLE);
	}
	//btnCtrl onClickListener shows Settings fragment
	@Test 
	public void btnSettingsOnClickListener_controllerFragment_visible(){
		Button btnSettings = (Button) splashView.findViewById(R.id.btnSettings);	
		btnSettings.performClick();	
		
		Fragment settingsFragment =  activity.getFragmentManager().findFragmentByTag(SettingsFragment.TAG);
		View settingsView = settingsFragment.getView().findViewById(R.id.settings_fragment);
		
		assertEquals(settingsView.getVisibility(), View.VISIBLE);
	}
	
	//btnExit visible
	@Test
	public void btnExit_onCreate_visible(){
		Button btnKeyboard = (Button) splashView.findViewById(R.id.btnExit);	
		assertEquals(btnKeyboard.getVisibility(), Button.VISIBLE);
	}
	

}
