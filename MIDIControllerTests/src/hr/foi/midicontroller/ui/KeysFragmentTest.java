package hr.foi.midicontroller.ui;


import static org.junit.Assert.*;

import hr.foi.midicontroller.R;
import hr.foi.midicontroller.bridge.IMidiBridge;
import hr.foi.midicontroller.core.MidiMessage;
import hr.foi.midicontroller.mocks.MidiBridgeMock;
import hr.foi.midicontroller.service.IService;
import hr.foi.midicontroller.stubs.IoioOutServiceStub;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.util.ActivityController;

import android.app.Fragment;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;
@RunWith(RobolectricTestRunner.class)
public class KeysFragmentTest {
	//main activity
	private MainActivity activity;
	
	//fragments
	Fragment splashFragment;
	Fragment keysFragment;
	
	//views
	 View splashView;
	 View keysView;
	 
	//action buttons
	private ToggleButton btnConnected;
	private Button 	btnBack, btnPitch,btnModulation,
					btnPoly, btnMode, btnI, btnII,
					btnIII, btnIV, btnV, btnC, btnCS,
					btnD, btnDS, btnE, btnF, btnFS, btnG,
					btnGS, btnA, btnAS, btnH;
	
	//transporter buttons
	private Button 	btnPlay, btnPause, btnStop, 
					btnRecord, btnPrev, btnNext;
	
	
	IMidiBridge bridge;
	IService service;
	ActivityController<MainActivity> controller;
	
	@Before
	public void setup() throws Exception{
		 controller = Robolectric.buildActivity( MainActivity.class).create();
		 activity =  (MainActivity)controller.get();
		 
		 //settings mocks and stubs
		 bridge = new MidiBridgeMock(100);
		 service = new IoioOutServiceStub();
		 activity.app.setBridge(bridge);
		 activity.app.setService(service);
		 
		 this.splashFragment =  activity.getFragmentManager().findFragmentByTag(SplashFragment.TAG);
	     this.splashView = splashFragment.getView().findViewById(R.id.splash_fragment);
		 Button btnKeyboard = (Button) splashView.findViewById(R.id.btnKeyboard);
		 btnKeyboard.performClick();	
		 
		 this.keysFragment = activity.getFragmentManager().findFragmentByTag(KeysFragment.TAG);
		 this.keysView = this.keysFragment.getView().findViewById(R.id.keys_fragment);

		 this.btnConnected = (ToggleButton) keysView.findViewById(R.id.btnConnected);
		 this.btnBack = (Button) keysView.findViewById(R.id.btnBack);
		 this.btnMode = (Button) keysView.findViewById(R.id.btnMode);
		 this.btnModulation = (Button) keysView.findViewById(R.id.btnModulation);
		 this.btnPitch = (Button) keysView.findViewById(R.id.btnPitch);
		 this.btnPoly  = (Button) keysView.findViewById(R.id.btnReset);
		 this.btnI = (Button) keysView.findViewById(R.id.btnOctave1);
		 this.btnII = (Button) keysView.findViewById(R.id.btnOctave2);
		 this.btnIII  = (Button) keysView.findViewById(R.id.btnOctave3);
		 this.btnIV  = (Button) keysView.findViewById(R.id.btnOctave4);
		 this.btnV = (Button) keysView.findViewById(R.id.btnOctave5);
		 
		 this.btnC  = (Button) keysView.findViewById(R.id.btnC);
		 this.btnCS = (Button) keysView.findViewById(R.id.btnCSharp);
		 this.btnD = (Button) keysView.findViewById(R.id.btnD);
		 this.btnDS = (Button) keysView.findViewById(R.id.btnDSharp);
		 this.btnE = (Button) keysView.findViewById(R.id.btnE);
		 this.btnF = (Button) keysView.findViewById(R.id.btnF);
		 this.btnFS = (Button) keysView.findViewById(R.id.btnFSharp);
		 this.btnG = (Button) keysView.findViewById(R.id.btnG);
		 this.btnGS = (Button) keysView.findViewById(R.id.btnGSharp);
		 this.btnA = (Button) keysView.findViewById(R.id.btnA);
		 this.btnAS = (Button) keysView.findViewById(R.id.btnASharp);
		 this.btnH = (Button) keysView.findViewById(R.id.btnH);

		 this.btnPlay = (Button) keysView.findViewById(R.id.btnPlay);
		 this.btnPause = (Button) keysView.findViewById(R.id.btnPause);
		 this.btnStop = (Button) keysView.findViewById(R.id.btnStop);
		 this.btnRecord = (Button) keysView.findViewById(R.id.btnRecord);
		 this.btnPrev = (Button) keysView.findViewById(R.id.btnPrev);
		 this.btnNext = (Button) keysView.findViewById(R.id.btnNext);			
	}
		
	@Test
	public void keysFragment_isNotNull(){
		assertNotNull( activity.getFragmentManager().findFragmentByTag(KeysFragment.TAG));
	}	
	 
	//btnBack visible
	@Test
	public void onCreate_btnBack_visible(){
		Fragment fragment =  activity.getFragmentManager().findFragmentByTag(KeysFragment.TAG);
		View keysView = fragment.getView().findViewById(R.id.keys_fragment);
		Button btnBack = (Button) keysView.findViewById(R.id.btnBack);	
		assertEquals(btnBack.getVisibility(), Button.VISIBLE);
	}
	
	//btnBack onClickListener shows splash fragment
	@Test 
	public void onClickListener_btnBackClicked_splashFragmentVisible(){
		this.btnBack.performClick();	
		
		Fragment fragmentSplash =  activity.getFragmentManager().findFragmentByTag(SplashFragment.TAG);
		View splashView = fragmentSplash.getView().findViewById(R.id.splash_fragment);	
		
		assertEquals(splashView.getVisibility(), View.VISIBLE);
	}
	
	//button connected visible
	@Test
	public void onCreate_btnConnected_visible(){
		assertEquals(Button.VISIBLE, btnConnected.getVisibility());
	}
	
	//Broadcasts aren't working with Robolectric framework so it's not possible
	//to test btnConnected status on connect and disconnect
	@Test
	public void onCreate_btnConnectedStatus_off(){
		assertFalse(btnConnected.isChecked());
	}
	
	//OCTAVE switch I,II,III, IV, V
	//On start button III is chosen
	@Test
	public void onCreate_chosenOctaveIsIII_octaveValue3(){
		assertEquals(3, activity.app.getOctave());
	}
	

	@Test
	public void onCreate_chosenOctaveIsIII_octaveTextColorIsWHITE(){
		controller.start().resume();
		assertEquals(Color.WHITE, btnIII.getCurrentTextColor());
	}
	//Octave 1
	@Test
	public void onOctave1Click_chosenOctave_1(){
		btnI.performClick();
		assertEquals(1, activity.app.getOctave());
	}
	

	@Test
	public void onOctave1Click_chosenOctave1Text_white(){
		controller.start().resume();
		btnI.performClick();
		assertEquals(Color.WHITE, btnI.getCurrentTextColor());
	}

	@Test
	public void onOctave1Click_previousChosenOctave3Text_black(){
		controller.start().resume();
		btnI.performClick();
		assertEquals(Color.BLACK, btnIII.getCurrentTextColor());
	}
	//Octave 2
	@Test
	public void onOctave2Click_chosenOctave_2(){
		btnII.performClick();
		assertEquals(2, activity.app.getOctave());
	}
	

	@Test
	public void onOctave2Click_chosenOctave2Text_white(){
		controller.start().resume();
		btnII.performClick();
		assertEquals(Color.WHITE, btnII.getCurrentTextColor());
	}

	@Test
	public void onOctave2Click_previousChosenOctave3Text_black(){
		controller.start().resume();
		btnII.performClick();
		assertEquals(Color.BLACK, btnIII.getCurrentTextColor());
	}
	
	//Octave 3
	@Test
	public void onOctave3Click_chosenOctave_3(){
		controller.start().resume();
		btnI.performClick();
		btnIII.performClick();
		assertEquals(3, activity.app.getOctave());
	}
	

	@Test
	public void onOctave3Click_chosenOctave3Text_white(){
		controller.start().resume();
		btnI.performClick();
		btnIII.performClick();
		assertEquals(Color.WHITE, btnIII.getCurrentTextColor());
	}

	@Test
	public void onOctave3Click_previousChosenOctave2Text_black(){
		controller.start().resume();
		btnI.performClick();
		btnIII.performClick();
		assertEquals(Color.BLACK, btnI.getCurrentTextColor());
	}
	//Octave 4
	@Test
	public void onOctave4Click_chosenOctave_4(){
		btnIV.performClick();
		assertEquals(4, activity.app.getOctave());
	}
	

	@Test
	public void onOctave4Click_chosenOctave4Text_white(){
		controller.start().resume();
		btnIV.performClick();
		assertEquals(Color.WHITE, btnIV.getCurrentTextColor());
	}

	@Test
	public void onOctave4Click_previousChosenOctave3Text_black(){
		controller.start().resume();
		btnIV.performClick();
		assertEquals(Color.BLACK, btnIII.getCurrentTextColor());
	}
	//Octave 5
	@Test
	public void onOctave5Click_chosenOctave_5(){
		btnV.performClick();
		assertEquals(5, activity.app.getOctave());
	}
	

	@Test
	public void onOctave5Click_chosenOctave5Text_white(){
		controller.start().resume();
		btnV.performClick();
		assertEquals(Color.WHITE, btnV.getCurrentTextColor());
	}

	@Test
	public void onOctave5Click_previousChosenOctave3Text_black(){
		controller.start().resume();
		btnV.performClick();
		assertEquals(Color.BLACK, btnIII.getCurrentTextColor());
	}
	

	//MMC Control buttons ***********************************
	//PLAY

	@Test
	public void onCreate_btnPlay_visible(){
		assertEquals(Button.VISIBLE, btnPlay.getVisibility());
	}
	
	//PAUSE
	@Test
	public void onCreate_btnPause_visible(){
		assertEquals(Button.VISIBLE, btnPause.getVisibility());
	}
	
	//STOP
	@Test
	public void onCreate_btnStop_visible(){
		assertEquals(Button.VISIBLE, btnStop.getVisibility());
	}
	
	//NEXT
	@Test
	public void onCreate_btnNext_visible(){
		assertEquals(Button.VISIBLE, btnNext.getVisibility());
	}
	//PREVIOUS
	@Test
	public void onCreate_btnPrev_visible(){
		assertEquals(Button.VISIBLE, btnPrev.getVisibility());
	}
	
	//RECORD
	@Test
	public void onCreate_btnRecord_visible(){
		assertEquals(Button.VISIBLE, btnRecord.getVisibility());
	}
	
	
	
}

















