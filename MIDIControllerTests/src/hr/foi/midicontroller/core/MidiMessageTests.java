package hr.foi.midicontroller.core;

import hr.foi.midicontroller.core.MidiMessage.MidiType;
import hr.foi.midicontroller.exceptions.MidiMessageException;

import org.robolectric.RobolectricTestRunner; 
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;


@RunWith(RobolectricTestRunner.class)
public class MidiMessageTests 
{

	@Test
	public void setMessageType_setNoteOn_MessageType144()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.NOTE_ON);
		
		assertEquals("Note on message type is not as expected value  144!",144, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setInvalidType_MessageType0()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.INVALID_TYPE);
		
		assertEquals("Note on message type is not as expected value  0!",0, message.getMessageType().getValue());
	}
	
	
	@Test
	public void setMessageType_setNoteOff_MessageType128()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.NOTE_OFF);
		
		assertEquals("Note on message type is not as expected value  128!",128, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setAfterTouchPoly_MessageType160()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.AFTERTOUCH_POLY);
		
		assertEquals("Note on message type is not as expected value  160!",160, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setControlChange_MessageType176()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.CONTROL_CHANGE);
		
		assertEquals("Note on message type is not as expected value  176!",176, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setProgramChange_MessageType192()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.PROGRAM_CHANGE);
		
		assertEquals("Note on message type is not as expected value  192!",192, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setAftertouchChannel_MessageType208()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.AFTERTOUCH_CHANNEL);
		
		assertEquals("Note on message type is not as expected value  208!",208, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setPitchBend_MessageType224()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.PITCH_BEND);
		
		assertEquals("Note on message type is not as expected value  224!",224, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setSystemExclusive_MessageType240()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.SYSTEM_EXCLUSIVE);
		
		assertEquals("Note on message type is not as expected value  240!",240, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setTimeCodeQuarterFrame_MessageType241()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.TIMECODE_QUARTERFRAME);
		
		assertEquals("Note on message type is not as expected value  241!",241, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setSongPosition_MessageType242()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.SONG_POSITION);
		
		assertEquals("Note on message type is not as expected value  242!",242, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setSongSelect_MessageType243()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.SONG_SELECT);
		
		assertEquals("Note on message type is not as expected value  243!",243, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setTuneRequest_MessageType246()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.TUNE_REQUEST);
		
		assertEquals("Note on message type is not as expected value  246!",246, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setTimingClock_MessageType248()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.TIMING_CLOCK);
		
		assertEquals("Note on message type is not as expected value  248!",248, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setStart_MessageType250()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.START);
		
		assertEquals("Note on message type is not as expected value  250!",250, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setContinue_MessageType251()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.CONTINUE);
		
		assertEquals("Note on message type is not as expected value  251!",251, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setStop_MessageType252()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.STOP);
		
		assertEquals("Note on message type is not as expected value  252!",252, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setActiveSensing_MessageType254()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.ACTIVE_SENSING);
		
		assertEquals("Note on message type is not as expected value  254!",254, message.getMessageType().getValue());
	}
	
	@Test
	public void setMessageType_setSystemReset_MessageType255()
	{
		MidiMessage message= new MidiMessage();
		message.setMessageType(MidiMessage.MidiType.SYSTEM_RESET);
		
		assertEquals("Note on message type is not as expected value  255!", 255, message.getMessageType().getValue());
	}
	
	
	@Test
	public void setData1_setMinimumValue_Zero() throws Exception
	{
		MidiMessage message = new MidiMessage();
		message.setByte1(0);
		
		assertEquals("Minimum value is not 0", 0, message.getByte1());
	}
	
	@Test
	public void setData1_setMaximumlValue_127() throws Exception
	{
		MidiMessage message = new MidiMessage();
		message.setByte1(127);
		
		assertEquals("Maximum value is not 127",127, message.getByte1());
	}
	
	@Test
	public void setData1_setBiggerThanMaximumValue_MidiMessageException() {
		try {
			MidiMessage message = new MidiMessage();
			message.setByte1(128);			
			fail("Does not throw exception exceed maximum value");			
		} catch (MidiMessageException e) {
			assertEquals("Byte data is larger than expected value of 127!", e.getMessage());
		}		
	}
	
	@Test
	public void setData1_setSmallerThanMinimumValue_MidiMessageException() {
		try {
			MidiMessage message = new MidiMessage();
			message.setByte1(-1);			
			fail("Does not throw exception exceed minimum value");			
		} catch (MidiMessageException e) {
			assertEquals("Byte data is smaller than expected value of 0!", e.getMessage());
		}		
	}
	
	@Test
	public void setData2_setMinimumValue_Zero() throws Exception{
			MidiMessage message = new MidiMessage();
			message.setByte2(0);			
			
			assertEquals("Minimum value is not 0", 0, message.getByte2());
	}
	
	@Test
	public void setData2_setMaximumlValue_127() throws MidiMessageException
	{
		MidiMessage message = new MidiMessage();
		message.setByte2(127);
		
		assertEquals("Maximum value is not 127",127, message.getByte2());
	}
	@Test
	public void setData2_setBiggerThanMaximumValue_MidiMessageException() {
		try {
			MidiMessage message = new MidiMessage();
			message.setByte2(129);			
			fail("Does not throw exception exceed maximum value");			
		} catch (MidiMessageException e) {
			assertEquals("Byte data is larger than expected value of 127!", e.getMessage());
		}		
	}
	 
	@Test
	public void setData2_setSmallerThanMinimumValue_MidiMessageException() {
		try {
			MidiMessage message = new MidiMessage();
			message.setByte2(-2);			
			fail("Does not throw exception exceed minimum value");			
		} catch (MidiMessageException e) {
			assertEquals("Byte data is smaller than expected value of 0!", e.getMessage());
		}		
	}
	
	@Test
	public void setSysExBytesData_setTenByteData_tenByteData() throws Exception
	{
		MidiMessage message = new MidiMessage();		
		byte data[] = { (byte) 0xF0, 0x41, 0x36, 0x06, 0x21, 0x20, 0x01, 0x22, 0x1B, (byte) 0xF7 };		
		message.setSysExBytes(data);
		
		assertEquals("Byte data is not as expected", data, message.getSysExBytes());
	}
	
	@Test
	public void setSysExBytesData_setNullData_throwsMidiMessageException() 
	{
		try{
			MidiMessage message = new MidiMessage();		
			byte[]  data = null;		
			message.setSysExBytes(data);	
			fail("Does not throw MidiMessageException!");	
		} catch (MidiMessageException e) {
			
		}
	}
	
	@Test
	public void isValid_validNoteOnMessage_true() throws MidiMessageException
	{
		MidiMessage message = new MidiMessage();		
		message.setMessageType(MidiMessage.MidiType.NOTE_ON);
		message.setByte1(60);
		message.setByte2(127);
						
		assertEquals(true, message.isValid());		
	}
	
	@Test
	public void isValid_invalidNoteOnMessageByte1BiggerValue_false() 
	{
		MidiMessage message = new MidiMessage();
		try {					
			message.setMessageType(MidiMessage.MidiType.NOTE_ON);
			message.setByte1(420);
			message.setByte2(127);
			fail("Does not throw MidiMessageException!");	
		}catch (MidiMessageException e) {
			assertEquals(false, message.isValid());		
		}		
	}
	
	@Test
	public void isValid_invalidNoteOnMessageByte2SmallerValue_false() 
	{
		MidiMessage message = new MidiMessage();
		try {					
			message.setMessageType(MidiMessage.MidiType.NOTE_ON);
			message.setByte1(20);
			message.setByte2(-127);
			fail("Does not throw MidiMessageException!");	
		}catch (MidiMessageException e) {
			assertEquals(false, message.isValid());		
		}		
	}
	
	@Test
	public void isValid_invalidSysExMessage_false() 
	{
		MidiMessage message = new MidiMessage();
		try {					
			message.setMessageType(MidiMessage.MidiType.SYSTEM_EXCLUSIVE);
			byte[]  data = null;		
			message.setSysExBytes(data);	

			fail("Does not throw MidiMessageException!");	
		}catch (MidiMessageException e) {
			assertEquals(false, message.isValid());		
		}		
	}
	
	@Test
	public void constructorOneByte_programChange_data1ShoudReturn120() throws Exception{
		
		MidiMessage message = new MidiMessage(MidiMessage.MidiType.PROGRAM_CHANGE, 120);
		assertEquals(message.getByte1(),120);
	}
	
	@Test
	public void constructorOneByte_programChangeWithDataBiggerThanExpected_throwsException() {
		try{
			@SuppressWarnings("unused")
			MidiMessage message = new MidiMessage(MidiMessage.MidiType.PROGRAM_CHANGE, 258);			
			fail("Does not throw MidiMessageException!");	
		}catch (MidiMessageException e) {
				
		}
	}
	
	@Test
	public void constructorOneByte_programChangeWirhSmallerDataThanExpected_throwsException() {
		try{
			@SuppressWarnings("unused")
			MidiMessage message = new MidiMessage(MidiMessage.MidiType.PROGRAM_CHANGE, -250);			
			fail("Does not throw MidiMessageException!");	
		}catch (MidiMessageException e) {
			
		}		
	}
	
	@Test
	public void constructorTwoBytes_noteOff_data1Is60() throws Exception{
		MidiMessage message = new MidiMessage(MidiType.NOTE_OFF, 60, 80);
		assertEquals(message.getByte1(),60);
	}
	
	@Test
	public void constructorTwoBytes_noteOff_data2Is80() throws Exception{
		MidiMessage message = new MidiMessage(MidiType.NOTE_OFF, 60, 80);
		assertEquals(message.getByte2(),80);
	}
	
	@Test
	public void calculateMessageToByteArray_noteOn_messageHexValue943C7F() throws Exception{
		MidiMessage message = new MidiMessage(MidiType.NOTE_ON, 60, 127, 5);
		assertArrayEquals(new byte[] { (byte)0x94, 0x3c, 0x7f}, message.calculateMessageToByteArray());
	}
	
	@Test
	public void  calculateMessageToByteArray_noteOnChannel16_messageHexValue9F3C7F() throws Exception{
		MidiMessage message = new MidiMessage(MidiType.NOTE_ON, 60, 127, 16);
		assertArrayEquals(new byte[] { (byte)0x9F, 0x3c, 0x7f}, message.calculateMessageToByteArray());
	}
	
	@Test
	public void  calculateMessageToByteArray_noteOnChannel1_messageHexValue903C7F() throws Exception{
		MidiMessage message = new MidiMessage(MidiType.NOTE_ON, 60, 127, 1);
		assertArrayEquals(new byte[] { (byte)0x90, 0x3c, 0x7f}, message.calculateMessageToByteArray());
	}
	
	@Test
	public void  calculateMessageToByteArray_noteOnIncorrectChannel17_null() throws Exception{
		MidiMessage message = new MidiMessage(MidiType.NOTE_ON, 60, 127, 17);
		assertArrayEquals(null, message.calculateMessageToByteArray());
	}
	
	@Test
	public void  calculateMessageToByteArray_noteOnIncorrectChannelMinus1_null() throws Exception{
		MidiMessage message = new MidiMessage(MidiType.NOTE_ON, 60, 127, 17);
		assertArrayEquals(null, message.calculateMessageToByteArray());
	}
	
	@Test
	public void  calculateMessageToByteArray_controlChangeChannel5_messageHexValueB43C7F() throws Exception{
		MidiMessage message = new MidiMessage(MidiType.CONTROL_CHANGE, 60, 127, 5);
		assertArrayEquals(new byte[] { (byte)0xb4, 0x3c, 0x7f}, message.calculateMessageToByteArray());
	}
	

	@Test
	public void  calculateMessageToByteArray_programChangeChannel5_messageHexValueC46F() throws Exception{
		MidiMessage message = new MidiMessage(MidiType.PROGRAM_CHANGE, 111);
		message.setChannel(5);
		assertArrayEquals(new byte[] { (byte)0xc4, 0x6f}, message.calculateMessageToByteArray());
	}
	

	@Test
	public void  calculateMessageToByteArray_emptyMessage_null() throws Exception{
		MidiMessage message = new MidiMessage();
		assertArrayEquals(null, message.calculateMessageToByteArray());
	}
}
