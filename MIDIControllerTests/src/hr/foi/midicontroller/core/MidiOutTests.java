package hr.foi.midicontroller.core;

import hr.foi.midicontroller.bridge.IMidiBridge;
import hr.foi.midicontroller.bridge.IoioBridge;
import hr.foi.midicontroller.core.MidiMessage.MidiType;
import hr.foi.midicontroller.exceptions.MidiMessageException;
import hr.foi.midicontroller.mocks.MidiBridgeMock;

import org.robolectric.RobolectricTestRunner; 
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;


@RunWith(RobolectricTestRunner.class)
public class MidiOutTests {
	private IMidiBridge bridge;
	private MidiOut out;
	
	@Before
	public void setup() {
		bridge = new MidiBridgeMock(100);
		out = new MidiOut(bridge);
	}
	
	@After
	public void teardown(){
		bridge.clearBuffer();
	}
	 
	@Test
	public void send_sendNoteOn_lastMessageHexValue943C7F() throws MidiMessageException{
		
		out.send(new MidiMessage(MidiType.NOTE_ON, 60, 127, 5));
		
		assertArrayEquals(new byte[] { (byte)0x94, 0x3c, 0x7f},out.getLastMessage().getHexValue());		
	}
	
	@Test
	public void send_sendNoteOn_true() throws MidiMessageException{

		boolean sent = out.send(new MidiMessage(MidiType.NOTE_ON, 60, 127, 5));
		
		assertEquals(true,sent);		
	}


	@Test
	public void send_sendNoteOn_deliveredToBuffer() throws MidiMessageException{

		MidiMessage message = new MidiMessage(MidiType.NOTE_ON, 60, 127, 5);
		
		out.send(message);
		
		assertArrayEquals(new byte[] { (byte)0x94, 0x3c, 0x7f},bridge.poll().getHexValue());		
	}
	

	
	@Test
	public void send_sendNoteOn_deliveredToBufferBufferIsEmptyAfter() throws MidiMessageException{

		MidiMessage message = new MidiMessage(MidiType.NOTE_ON, 60, 127, 5);
		
		out.send(message);
		bridge.poll().getHexValue();
		assertEquals(null,bridge.poll());		
	}
	
	@Test
	public void sendNoteOn_validNoteC_deliveredToBuffer() {

		out.sendOn(60, 127, 5);
		
		assertArrayEquals(new byte[] { (byte)0x94, 0x3c, 0x7f},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendNoteOn_validNoteC_true() throws MidiMessageException{

		boolean sent = out.sendOn(60, 127, 5);
		assertTrue(sent);				
	}
	
	@Test
	public void sendNoteOn_invalidNoteC_notDeliveredToBuffer() {

		out.sendOn(60, -11, 5);
		
		assertEquals(null,bridge.poll());				
	}
	
	@Test
	public void sendNoteOn_invalidNoteC_false() {

		boolean sent = out.sendOn(60, -11, 5);
		assertFalse(sent);			
	}
	
	@Test
	public void sendNoteOff_validNoteC_deliveredToBuffer() {

		out.sendOff(60, 0, 5);
		
		assertArrayEquals(new byte[] { (byte)0x84, 0x3c, 0x00},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendNoteOff_validNoteOffC_true() throws MidiMessageException{

		boolean sent = out.sendOff(60, 0, 5);
		assertTrue(sent);				
	}
	
	@Test
	public void sendNoteOff_invalidOffNoteC_notDeliveredToBuffer() {

		out.sendOff(60, -11, 5);
		
		assertEquals(null,bridge.poll());				
	}
	
	@Test
	public void sendNoteOff_invalidOffNoteC_false() {

		boolean sent = out.sendOff(60, -11, 17);
		assertFalse(sent);			
	}
	

	@Test
	public void sendAfterTouchPoly_validNoteC_deliveredToBuffer() {

		out.sendAfterTouchPoly(60, 0, 5);
		
		assertArrayEquals(new byte[] { (byte)0xA4, 0x3c, 0x00},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendAfterTouchPoly_validNoteC_true() throws MidiMessageException{

		boolean sent = out.sendAfterTouchPoly(60, 0, 5);
		assertTrue(sent);				
	}
	
	@Test
	public void sendAfterTouchPoly_invalidNoteC_notDeliveredToBuffer() {

		out.sendAfterTouchPoly(60, -11, 5);
		
		assertEquals(null,bridge.poll());				
	}
	
	@Test
	public void sendAfterTouchPoly_invalidNoteC_false() {

		boolean sent = out.sendAfterTouchPoly(60, -11, 17);
		assertFalse(sent);			
	}
 
	 
	@Test
	public void sendAControlChange_validCC_deliveredToBuffer() {

		out.sendControlChange(111, 50, 5);
		
		assertArrayEquals(new byte[] { (byte)0xB4, 0x6F, 0x32},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendAControlChange_validC_true() throws MidiMessageException{

		boolean sent = out.sendControlChange(111, 50, 5);
		assertTrue(sent);				
	}
	
	@Test
	public void sendAControlChange_invalidCC_notDeliveredToBuffer() {

		out.sendControlChange(256, 50,17);
		
		assertEquals(null,bridge.poll());				
	}
	
	@Test
	public void sendAControlChange_invalidCC_false() {

		boolean sent = out.sendControlChange(-1, 50, -1);
		assertFalse(sent);			
	}
 	 	 

	@Test
	public void sendProgramChange_validProgram_deliveredToBuffer(){

		out.sendProgramChange(10, 5);
		
		assertArrayEquals(new byte[] { (byte)0xC4, (byte)0x0a},bridge.poll().getHexValue());				
	}
	
	@Test
	public void  sendProgramChange_validProgram_true() {

		boolean sent = out.sendProgramChange(10, 5);
		assertTrue(sent);				
	}
	
	@Test
	public void  sendProgramChange_invalidProgram_notDeliveredToBuffer() {

		out.sendProgramChange(-10, 5);
		
		assertEquals(null,bridge.poll());				
	}
	
	@Test
	public void sendProgramChange_invalidProgram_false() {

		boolean sent = out.sendProgramChange(128, 5);
		assertFalse(sent);			
	}
 		
	@Test
	public void sendAfterTouchChannel_valid_deliveredToBuffer()  {

		out.sendAfterTouchChannel(60, 5);
		
		assertArrayEquals(new byte[] { (byte)0xD4, 0x3c},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendAfterTouchChannel_valid_true() {

		boolean sent = out.sendAfterTouchChannel(60, 5);
		assertTrue(sent);				
	}
	
	@Test
	public void sendAfterTouchChannel_invalid_notDeliveredToBuffer()  {

		out.sendAfterTouchChannel(-60, 5);
		
		assertEquals(null,bridge.poll());				
	}
	
	@Test
	public void sendAfterTouchChannel_invalid_false()  {

		boolean sent = out.sendAfterTouchChannel(60,  17);
		assertFalse(sent);			
	}
 	 	 
	@Test
	public void sendPitchBend_valid_deliveredToBuffer() {

		out.sendPitchBend(1.0f, 5);
		
		assertArrayEquals(new byte[] { (byte)0xE4, 0x7f, 0x7f},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendPitchBend_valid_true(){

		boolean sent = out.sendPitchBend(0.5f, 5);
		assertTrue(sent);				
	}
	
	@Test
	public void sendPitchBend_invalid_notDeliveredToBuffer(){

		out.sendPitchBend(-1.1f, 15);
		
		assertEquals(null,bridge.poll());				
	}
	@Test
	public void sendPitchBend_invalidChannell_notDeliveredToBuffer(){

		out.sendPitchBend(1f, 17);
		
		assertEquals(null,bridge.poll());				
	}
	
	
	@Test
	public void sendPitchBend_invalid_false() {

		//#define MIDI_PITCHBEND_MIN      -8192
		//#define MIDI_PITCHBEND_MAX      8191
		boolean sent = out.sendPitchBend(1.01f, 15);
		assertFalse(sent);			
	}
 	 	 	 
	@Test
	public void sendTimeCodeQuarterFrame_valid_deliveredToBuffer() {

		out.sendTimeCodeQuarterFrame(120);
		
		assertArrayEquals(new byte[] { (byte)0xF1, 0x78},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendTimeCodeQuarterFrame_valid_true() {

		boolean sent = out.sendTimeCodeQuarterFrame(120);
		assertTrue(sent);				
	}
	
	@Test
	public void sendTimeCodeQuarterFrame_invalid_notDeliveredToBuffer() {

		out.sendTimeCodeQuarterFrame(130);
		
		assertEquals(null,bridge.poll());				
	}
	
	@Test
	public void sendTimeCodeQuarterFrame_invalid_false() {

		boolean sent = out.sendTimeCodeQuarterFrame(-1);
		assertFalse(sent);			
	}
 	 	 	 
	@Test
	public void sendSongPosition_valid_deliveredToBuffer() {

		out.sendSongPosition(50, 60);
		
		assertArrayEquals(new byte[] { (byte)0xf2, 0x32, 0x3c},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendSongPosition_valid_true() throws MidiMessageException{

		boolean sent = out.sendSongPosition(50, 60);
		assertTrue(sent);				
	}
	
	@Test
	public void sendSongPosition_invalid_notDeliveredToBuffer() {

		out.sendSongPosition(2580, 60);
		
		assertEquals(null,bridge.poll());				
	}
	
	@Test
	public void sendSongPosition_invalid_false() {

		boolean sent = out.sendSongPosition(-11, 60);
		assertFalse(sent);			
	}
 	 	
	
	@Test
	public void sendSongSelect_valid_deliveredToBuffer() {

		out.sendSongSelect(60);
		
		assertArrayEquals(new byte[] { (byte)0xF3, 0x3c},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendSongSelect_valid_true() {

		boolean sent = out.sendSongSelect(60);
		assertTrue(sent);				
	}
	
	@Test
	public void sendSongSelect_invalid_notDeliveredToBuffer() {

		out.sendSongSelect(260);
		
		assertEquals(null,bridge.poll());				
	}
	
	@Test
	public void sendSongSelect_invalid_false() {

		boolean sent = out.sendSongSelect(-1);
		assertFalse(sent);			
	}
 	

	@Test
	public void sendTuneRequest_valid_deliveredToBuffer() {

		out.sendTuneRequest();
		
		assertArrayEquals(new byte[] { (byte)0xF6,},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendTuneRequest_valid_true() throws MidiMessageException{

		boolean sent = out.sendTuneRequest();
		assertTrue(sent);				
	}
	
 	 	 
	@Test
	public void sendTimingClock_valid_deliveredToBuffer() {

		out.sendTimingClock();
		
		assertArrayEquals(new byte[] { (byte)0xF8},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendTimingClock_valid_true() {

		boolean sent = out.sendTimingClock();
		assertTrue(sent);				
	}
	
	
	@Test
	public void sendStart_valid_deliveredToBuffer() {

		out.sendStart();		
		assertArrayEquals(new byte[] { (byte)0xFA},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendStart_valid_true() {

		boolean sent = out.sendStart();
		assertTrue(sent);				
	}
	
	 
	@Test
	public void sendContinue_valid_deliveredToBuffer() {

		out.sendContinue();
		
		assertArrayEquals(new byte[] { (byte)0xFB},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendContinue_valid_true() {

		boolean sent = out.sendContinue();
		assertTrue(sent);				
	}
	

	@Test
	public void sendStop_valid_deliveredToBuffer() {

		out.sendStop();
		
		assertArrayEquals(new byte[] { (byte)0xFC},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendStop_valid_true() {

		boolean sent = out.sendStop();
		assertTrue(sent);				
	}
	 

	@Test
	public void sendActiveSensing_valid_deliveredToBuffer() {

		out.sendActiveSensing();
		
		assertArrayEquals(new byte[] { (byte)0xFE},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendActiveSensing_valid_true() {

		boolean sent = out.sendActiveSensing();
		assertTrue(sent);				
	}
	
	@Test
	public void sendSystemReset_valid_deliveredToBuffer() {

		out.sendSystemReset();
		
		assertArrayEquals(new byte[] { (byte)0xFF},bridge.poll().getHexValue());				
	}
	
	@Test
	public void sendSystemReset_valid_true() {

		boolean sent = out.sendSystemReset();
		assertTrue(sent);				
	}

	@Test
	public void sendSystemReset_connect_true() {
		IMidiBridge bridge = new IoioBridge(100);
		MidiOut mout = new MidiOut(bridge);
		boolean sent = mout.sendSystemReset();
		assertTrue(sent);				
	}
}






























