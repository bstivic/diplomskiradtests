package hr.foi.midicontroller.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class MidiHelperTests {
	@Test
	public void calculateOctaveTranspose_noteCOctave3_60()
	{
		MidiHelper helper= new MidiHelper();
		int transpose = helper.calculateOctaveTranspose(60, 3);
		
		assertEquals("Note on message type is not as expected value  60!",60, transpose);
	}
	
	@Test
	public void calculateOctaveTranspose_noteCOctave4_72()
	{
		MidiHelper helper= new MidiHelper();
		int transpose = helper.calculateOctaveTranspose(60, 4);
		
		assertEquals("Note on message type is not as expected value  72!",72, transpose);
	}
	
	@Test
	public void calculateOctaveTranspose_noteCOctave5_84()
	{
		MidiHelper helper= new MidiHelper();
		int transpose = helper.calculateOctaveTranspose(60,5);
		
		assertEquals("Note on message type is not as expected value  84!",84, transpose);
	}
	
	@Test
	public void calculateOctaveTranspose_noteDSOctave5_51()
	{
		MidiHelper helper= new MidiHelper();
		int transpose = helper.calculateOctaveTranspose(75,1);
		
		assertEquals("Note on message type is not as expected value  51!",51, transpose);
	}
	
	@Test
	public void calculateOctaveTranspose_noteCOctave7_84()
	{
		MidiHelper helper= new MidiHelper();
		int transpose = helper.calculateOctaveTranspose(60,5);
		
		assertEquals("Note on message type is not as expected value  84!",84, transpose);
	}
	
}
